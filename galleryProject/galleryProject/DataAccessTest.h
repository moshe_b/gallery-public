#pragma once
#include "DatabaseAccess.h"
#include "sqlite3.h"
#include "Album.h"
#include <list>


bool creatDB();
void deleteAlbum();
void closeDB();
void addPictureToAlbumByName();
void removePictureFromAlbumByName();
void tagUserInPicture();
void createUser();
void deleteUser();
void getAlbums();
void getAlbumsOfUser();