#include "Server.h"
#include <exception>
#include <iostream>
#include <string>

#pragma warning(disable : 4996)

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	_albumManager = new AlbumManager(_da);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	clientHandler(client_socket);
}


void Server::clientHandler(SOCKET clientSocket)
{
	try
	{
		Helper::sendData(clientSocket, "connect");
		while (true)
		{
			printf("in while");			
			char buf[1024] = { 0 };
			recv(clientSocket, buf, 1024, 0); //get all the message

			if (std::string(buf) == "")
				break;
			printf("soemthoing");
			std::pair<CommandType, std::vector<std::string>> message_from_client = Helper::receivData(buf);
			std::cout << buf << std::endl;

			std::pair<CommandType, std::vector<std::string>> message_to_client = this->_albumManager->executeCommand(message_from_client.first
									, message_from_client.second);

			for (int i = 0; i < message_to_client.second.size(); i++)
				std::cout << message_to_client.second[i] << std::endl;
			Helper::send_message_by_protocol(clientSocket, message_to_client.first, message_to_client.second);
		}

		closesocket(clientSocket);
	}
	catch (const std::exception& e)
	{
		TRACE("close socket");
		std::cout << e.what() << std::endl;
		closesocket(clientSocket);
	}


}



