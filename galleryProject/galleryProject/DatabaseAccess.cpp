#include "DatabaseAccess.h"


bool DatabaseAccess::open()
{
	string dbFileName = "galleryProject.sqlite";

	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &_db);

	if (res != SQLITE_OK) {
		this->_db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}

	if (doesFileExist != 0)
	{
		//USERS
		const char* usersStatement = R"(CREATE TABLE USERS (ID INTEGER PRIMARY KEY,
                                        NAME TEXT NOT NULL);)";
		char** errMessage1 = nullptr;
		res = sqlite3_exec(this->_db, usersStatement, nullptr, nullptr, errMessage1);

		if (res != SQLITE_OK)
			return false;

		//ALBUMS
		const char* albumsStatement = R"(CREATE TABLE ALBUMS (
									   ID INTEGER PRIMARY KEY,
									   NAME TEXT NOT NULL, 
									   CREATION_DATE TEXT NOT NULL, 
                                       USER_ID INTEGER NOT NULL,
									   FOREIGN KEY(USER_ID) REFERENCES USERS(ID));)";
		char** errMessage2 = nullptr;
		res = sqlite3_exec(this->_db, albumsStatement, nullptr, nullptr, errMessage2);

		if (res != SQLITE_OK)
			return false;


		//PICTURES
		const char* picturesStatement = R"(CREATE TABLE PICTURES (
                                           ID INTEGER PRIMARY KEY,
										   NAME TEXT NOT NULL, 
										   LOCATION TEXT NOT NULL, 
										   CREATION_DATE TEXT NOT_NULL, 
										   ALBUM_ID INTEGER NOT NULL, 
										   FOREIGN KEY(ALBUM_ID) REFERENCES ALBUMS(ID));)";
		char** errMessage3 = nullptr;
		res = sqlite3_exec(this->_db, picturesStatement, nullptr, nullptr, errMessage3);

		if (res != SQLITE_OK)
			return false;
		
		//TAGS
		const char* tagsStatement = R"(CREATE TABLE TAGS(
									   ID int PRIMARY KEY,
									   PICTURE_ID  int NOT NULL,
									   USER_ID int NOT NULL,
									   FOREIGN KEY(PICTURE_ID) REFERENCES PICTURES(ID),
									   FOREIGN KEY(USER_ID) REFERENCES USERS(ID));)";
		char** errMessage4 = nullptr;
		res = sqlite3_exec(this->_db, tagsStatement, nullptr, nullptr, errMessage4);

		if (res != SQLITE_OK)
			return false;
	}

    return true;
}

void DatabaseAccess::close()
{
	sqlite3_close(this->_db);
	this->_db = nullptr;
}


void DatabaseAccess::clear()
{
	char** errMessage = nullptr;
	const char* DELETE_USERS = R"(DROP TABLE IF EXISTS USERS)";
	const char* DELETE_ALBUMS = R"(DROP TABLE IF EXISTS ALBUMS)";
	const char* DELETE_PICTURES = R"(DROP TABLE IF EXISTS PICTURES)";
	const char* DELETE_TAGS = R"(DROP TABLE IF EXISTS TAGS)";

	int resUsers = sqlite3_exec(this->_db, DELETE_USERS, nullptr, nullptr, errMessage);
	int resAlbums = sqlite3_exec(this->_db, DELETE_ALBUMS, nullptr, nullptr, errMessage);
	int resPictures = sqlite3_exec(this->_db, DELETE_PICTURES, nullptr, nullptr, errMessage);
	int resTags = sqlite3_exec(this->_db, DELETE_TAGS, nullptr, nullptr, errMessage);

	if (resUsers != SQLITE_OK || resAlbums != SQLITE_OK || 
		resPictures != SQLITE_OK || resTags != SQLITE_OK)
		throw std::runtime_error("cannot delete one of the tables");
}

int DatabaseAccess::getMaxId(kind_table kind)
{
	string query = "SELECT ID FROM USERS ORDER BY ID DESC LIMIT 1;";

	//int id
	return 0;
}

int getAmountCallBack(void* data, int argc, char** argv, char** azColName)
{
	int* value = (int*)data;
	if (argc > 0)
		*value = std::atoi(argv[0]);
	return 0;
}

int getPictureTagsCallBack(void* data, int argc, char** argv, char** azColName)
{
	std::set<int>* tags = reinterpret_cast<std::set<int>*>(data);
	int tag = 0;
	for (int i = 0; i < argc; i++)
	{
		if (string(argv[i]) == "ID")
			tag = std::atoi(argv[i]);
	}

	tags->insert(tag);
	return 0;
}

std::list<Picture>* DatabaseAccess::getPictures(std::string albumName)
{
	string query = "SELECT * FROM PICTURES WHERE ALBUM_ID = (SELECT ID FROM ALBUMS WHERE NAME = \""+ albumName+"\");";
	std::cout << "Album Name = " << albumName << "\n";
	std::list<Picture>* listPictures = new std::list<Picture>;
	int res = sqlite3_exec(this->_db, (const char*)query.c_str(), getPicturesCallBack, (void*)listPictures, nullptr);

	std::list<Picture>::iterator iter = listPictures->begin();
	for (iter; iter != listPictures->end(); iter++)
	{
		std::cout << *iter << "\n";
		//std::set<int> tags;
		//query = "SELECT * FROM FROM TAGS;";
		//sqlite3_exec(this->_db, (const char*)query.c_str(), getPictureTagsCallBack, (void*)&tags, nullptr);
		//iter->setTagged(tags);
	}

	if (res != SQLITE_OK)
		throw std::runtime_error("error in getPictures");

	return listPictures;
}

void DatabaseAccess::deleteTags(std::list<std::pair<int, int>> tagsPicIds)
{
	std::list<std::pair<int, int>>::iterator iter = tagsPicIds.begin();
	for (iter; iter != tagsPicIds.end(); iter++)
	{
		string query = "DELETE FROM TAGS WHERE ID = " + std::to_string(iter->second) + ";";
		sqlite3_exec(_db, query.c_str(), nullptr, nullptr, nullptr);
	}
}

void DatabaseAccess::deletePictures(std::list<std::pair<int, int>> tagsPicIds)
{
	std::list<std::pair<int, int>>::iterator iter = tagsPicIds.begin();
	for (iter; iter != tagsPicIds.end(); iter++)
	{
		string query = "DELETE FROM PICTURES WHERE ID = " + std::to_string(iter->first) + ";";
		sqlite3_exec(_db, query.c_str(), nullptr, nullptr, nullptr);
		query = "DELETE FROM TAGS WHERE PICTURE_ID = " + std::to_string(iter->second) + ";";
		sqlite3_exec(_db, query.c_str(), nullptr, nullptr, nullptr);
	}
}

std::set<int> DatabaseAccess::getTags(Picture pic)
{
	std::set<int> tags;
	string query = "SELECT ID FROM PICTURES WHERE ID = " + std::to_string(pic.getId()) + ";";

	std::cout << "Picture id " << pic.getId() << "\n";
	int id = -1;
	int res = sqlite3_exec(_db, query.c_str(), getAmountCallBack, &id, nullptr);

	std::cout << "id = " << id << "\n";
	if (id == -1)
		throw std::runtime_error("There is no picture in this name ");

	query = "SELECT USER_ID,PICTURE_ID FROM TAGS WHERE PICTURE_ID = " + std::to_string(id) + ";";
	std::list<std::pair<int, int>> listPairs;
	res = sqlite3_exec(_db, query.c_str(), getTagsPicCallBack, (void*)&listPairs, nullptr);

	for (std::list<std::pair<int, int>>::iterator iter = listPairs.begin();
		iter != listPairs.end(); iter++)
		tags.insert(iter->first);

	std::cout << "tags len = " << tags.size() + "\n";
	return tags;
}

int getAlbumsCallBack(void* data, int argc, char** argv, char** azColName)
{
	std::list<Album>& listAlbums = *reinterpret_cast<std::list<Album>*>(data);
	Album currAlbum(0, "");

	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "NAME")
		{
			currAlbum.setName(argv[i]);
		}
		else if (string(azColName[i]) == "CREATION_DATE")
		{
			currAlbum.setCreationDate(argv[i]);
		}
		else if (string(azColName[i]) == "USER_ID")
		{
			currAlbum.setOwner(std::atoi(argv[i]));
		}
	}

	listAlbums.push_back(currAlbum);
	return 0;
}

int getUsersCallBack(void* data, int argc, char** argv, char** azColName)
{
	std::list<User>* users = reinterpret_cast<std::list<User>*>(data);
	User user(0, "");

	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "NAME")
		{
			user.setName(argv[i]);
		}
		else if (string(azColName[i]) == "ID")
		{
			user.setId(std::atoi(argv[i]));
		}
	}

	users->push_back(user);
	return 0;
}

int getPicturesCallBack(void* data, int argc, char** argv, char** azColName)
{
	std::list<Picture>* listPictures = reinterpret_cast<std::list<Picture>*>(data);;
	Picture pic(0, "");

	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "ID")
		{
			pic.setId(std::atoi(argv[i]));
		}
		else if (string(azColName[i]) == "NAME")
		{
			pic.setName(string(argv[i]));
		}
		else if (string(azColName[i]) == "LOCATION")
		{
			pic.setPath(argv[i]);
		}
		else if (string(azColName[i]) == "CREATION_DATE")
		{
			pic.setCreationDate(string(argv[i]));
		}
	}


	listPictures->push_back(pic);
	return 0;
}

int getTagsPicCallBack(void* data, int argc, char** argv, char** azColName)
{
	std::list<std::pair<int, int>>* listPictures = reinterpret_cast<std::list<std::pair<int, int>>*>(data);
	std::pair<int, int> p;
	for (int i = 0; i < argc; i++)
	{
		if (i == 0)
			p.first = std::atoi(argv[i]);
		else
			p.second = std::atoi(argv[i]);
	}

	listPictures->push_back(p);
	return 0;
}

int getSomething(void* data, int argc, char** argv, char** azColName)
{
	int* value = (int*)data;
	*value = std::atoi(argv[0]);
	return 0;
}

DatabaseAccess::DatabaseAccess()
{
	this->open();
}

const std::list<Album> DatabaseAccess::getAlbums()
{
	string getAlbumsQuery = "SELECT * FROM ALBUMS;";
	std::list<Album> listAlbums;

	int res = sqlite3_exec(this->_db, (const char*)getAlbumsQuery.c_str(), getAlbumsCallBack, (void*)&listAlbums, nullptr);

	if (res != SQLITE_OK)
		throw std::runtime_error("error in getAlbums");
	return listAlbums;
}

const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
	string getAlbumsUserQuery = "SELECT * FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + ";";

	std::list<Album> listAlbums;

	int res = sqlite3_exec(this->_db, (const char*)getAlbumsUserQuery.c_str(), getAlbumsCallBack, (void*)&listAlbums, nullptr);

	if (res != SQLITE_OK)
		throw std::runtime_error("error in getAlbumsOfUser");
	return listAlbums;
}

void DatabaseAccess::createAlbum(const Album& album)
{
	std::list<Album> listAlbums = getAlbums();
	int id = !listAlbums.size() ? 0 : listAlbums.size()+1; //get the last id
	if (!doesAlbumExists(album.getName(), album.getOwnerId()))
	{
		string queryCreate = "INSERT INTO ALBUMS (NAME, CREATION_DATE, USER_ID) VALUES (\""
			+ album.getName() + "\", \"" + album.getCreationDate() + "\"," + std::to_string(album.getOwnerId()) + ");";

		int res = sqlite3_exec(this->_db, (const char*)queryCreate.c_str(), nullptr, nullptr, nullptr);
		if (res != SQLITE_OK)
			throw std::runtime_error("error in createAlbum");
	}
	
}

void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	//get all pairs of ids
	int res = 0;
	std::list<std::pair<int, int>> listPairs;
	string query = "SELECT PICTURES.ID, TAGS.ID FROM TAGS INNER JOIN PICTURES ON \
		PICTURES.ID = TAGS.PICTURE_ID WHERE PICTURES.ALBUM_ID = (SELECT ID FROM ALBUMS WHERE NAME = \"" + albumName + "\");";
	res = sqlite3_exec(this->_db, query.c_str(), getTagsPicCallBack, (void*)&listPairs, nullptr);
	//delete tags
	deleteTags(listPairs);
	//delete pictures
	deletePictures(listPairs);

	//delete the album itself
	string deleteMessage = "DELETE FROM ALBUMS WHERE USER_ID = " +
								 std::to_string(userId) + " AND NAME = \"" +albumName+ "\";";
    res = sqlite3_exec(this->_db, deleteMessage.c_str(), nullptr, nullptr, nullptr);
	if (res != SQLITE_OK)
	{
		throw std::runtime_error("deleteAlbum error");
	}
}

bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	string query = "SELECT * FROM ALBUMS WHERE USER_ID = " + std::to_string(userId) + " AND NAME = \"" + albumName + "\";";

	std::list<Album> listAlbums;

	int res = sqlite3_exec(this->_db, (const char*)query.c_str(), getAlbumsCallBack, (void*)&listAlbums, nullptr);

	if (res != SQLITE_OK)
		throw std::runtime_error("error in getAlbumsOfUser");

	return listAlbums.size() != 0;
}

Album DatabaseAccess::openAlbum(const std::string& albumName)
{
	string query = "SELECT * FROM ALBUMS WHERE NAME = \"" + albumName + "\";";
	std::list<Album> listAlbums;

	int res = sqlite3_exec(this->_db, (const char*)query.c_str(), getAlbumsCallBack, (void*)&listAlbums, nullptr);

	if (res != SQLITE_OK)
		throw std::runtime_error("error in openAlbum");

	if (listAlbums.size() == 0)
		throw std::runtime_error("The album name doesn't exists");

	std::list<Album>::iterator iter = listAlbums.begin(); //the requested album

	std::list<Picture> pictures = *getPictures(albumName);
	std::list<Picture>::iterator iterPic = pictures.begin();

	for (iterPic; iterPic != pictures.end(); iterPic++){
		iterPic->setTagged(getTags(*iterPic));
	}

	iter->setPictures(pictures);

	return *iter; //it's have only one album...
}

void DatabaseAccess::closeAlbum(Album& pAlbum) {}

std::vector<std::string>  DatabaseAccess::printAlbums()
{
	string query = "SELECT * FROM ALBUMS;";
	std::list<Album> listAlbums;
	std::vector<std::string> retAlbums;

	int res = sqlite3_exec(this->_db, (const char*)query.c_str(), getAlbumsCallBack, (void*)&listAlbums, nullptr);

	if (res != SQLITE_OK)
		throw std::runtime_error("error in printAlbums");

	std::string albumsStr = "";
	for (std::list<Album>::iterator iter = listAlbums.begin(); iter != listAlbums.end(); iter++) {
		std::ostringstream s;
		s << *iter;
		retAlbums.push_back(s.str());
	}

	return retAlbums;
}

void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	//need to add checking if the pciture is already exist(part 3)
	std::string addMessage = "INSERT INTO PICTURES (NAME, LOCATION, CREATION_DATE, ALBUM_ID) VALUES(\"" +
		picture.getName() + "\"," + "\"" + picture.getPath() + "\""+ ", \"" + picture.getCreationDate() + 
		            "\" , (SELECT ID FROM ALBUMS WHERE NAME = \""+ albumName + "\"));";
	int res = sqlite3_exec(this->_db, (const char*)addMessage.c_str(), nullptr, nullptr, nullptr);

	std::cout << "res = " << res << "\n";
	std::cout << addMessage << "\n";
	if (res != SQLITE_OK) 
		throw std::runtime_error("Error in addPictureToAlbumByName function");
}

void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	std::string removeMessage = "DELETE FROM PICTURES WHERE NAME = \"" + pictureName + "\" AND ALBUM_ID = " + 
									"(SELECT ID FROM ALBUMS WHERE NAME = \""+ albumName + "\");";
	int res = sqlite3_exec(this->_db, (const char*)removeMessage.c_str(), nullptr, nullptr, nullptr);

	if (res != SQLITE_OK)
	{
		throw std::runtime_error("Error in removePictureFromAlbumByName function");
	}
}

void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	string tagUser = "INSERT INTO TAGS VALUES((CASE WHEN (SELECT MAX(ID) FROM TAGS) IS NULL THEN 0 \
                     ELSE(SELECT MAX(ID) FROM TAGS)+1 END), \
				     (SELECT ID FROM PICTURES WHERE NAME = \"" +pictureName +
						"\"AND ALBUM_ID = (SELECT ALBUM_ID FROM ALBUMS WHERE NAME =" + "\"" + albumName +"\""+")),"+ 
						  std::to_string(userId) + ");";
	int res = sqlite3_exec(this->_db, (const char*)tagUser.c_str(), nullptr, nullptr, nullptr);

	if (res != SQLITE_OK) {
		throw std::runtime_error("Error in removePictureFromAlbumByName function");
	}
}

void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	//need to add it
	string getPicId = "SELECT ID FROM PICTURES WHERE NAME = \"" + pictureName + "\" AND ALBUM_ID = \
								(SELECT ID FROM ALBUMS WHERE NAME = \""+albumName +"\");";

	int picId = -1;
	int res = sqlite3_exec(_db, getPicId.c_str(), getAmountCallBack, &picId, nullptr);

	string untag = "DELETE FROM TAGS WHERE PICTURE_ID = " + std::to_string(picId) + " AND USER_ID = " + std::to_string(userId) + ";";
	res = sqlite3_exec(_db, untag.c_str(), nullptr, nullptr, nullptr);
}

std::vector<std::string> DatabaseAccess::printUsers()
{
	string query = "SELECT * FROM USERS;";
	std::list<User> users;

	int res = sqlite3_exec(this->_db, (const char*)query.c_str(), getUsersCallBack, (void*)&users, nullptr);

	if (res != SQLITE_OK) {
		throw std::runtime_error("Error in printUsers function");
	}
	std::list<User>::iterator iter;
	std::vector<std::string> listUsers;
	for (iter = users.begin(); iter != users.end(); iter++) {
		std::ostringstream oss;
		oss << *iter;
		listUsers.push_back(oss.str());
		std::cout << *iter << std::endl;
	}

	return listUsers;
}

void DatabaseAccess::createUser(User& user)
{
	//need to check if user already exist
	string createUser = "INSERT INTO USERS (NAME) VALUES (\"" +user.getName() +"\");";
	std::cout << createUser << std::endl;
	int res = sqlite3_exec(this->_db, (const char*)createUser.c_str(), nullptr, nullptr, nullptr);

	if (res != SQLITE_OK)
	{
		throw std::runtime_error("Error in removePictureFromAlbumByName function");
	}
}

void DatabaseAccess::deleteUser(const User& user)
{
	string deletePictures = "SELECT PICTURES.ID, PICTURES.NAME, PICTURES.LOCATION, PICTURES.CREATION_DATE, PICTURES.ALBUM_ID \
		FROM PICTURES INNER JOIN ALBUMS ON PICTURES.ALBUM_ID = ALBUMS.ID WHERE ALBUMS.USER_ID = " + std::to_string(user.getId()) + ";";
	std::list<Picture> listPicturesOfUser;
	sqlite3_exec(this->_db, deletePictures.c_str(), getPicturesCallBack, &listPicturesOfUser, nullptr);

	std::list<Picture>::iterator iter;
	for (iter = listPicturesOfUser.begin(); iter != listPicturesOfUser.end(); iter++)
	{
		string deleteTags = "DELETE FROM TAGS WHERE PICTURE_ID = " + std::to_string(iter->getId()) + ";";
		sqlite3_exec(this->_db, (const char*)deleteTags.c_str(), nullptr, nullptr, nullptr);

		string deletePictures = "DELETE FROM PICTURES WHERE ID = " + std::to_string(iter->getId()) + ";";
		sqlite3_exec(this->_db, (const char*)deletePictures.c_str(), nullptr, nullptr, nullptr);
	}

	string deleteUser = "DELETE FROM USERS WHERE ID = " + std::to_string(user.getId()) + ";";
	sqlite3_exec(this->_db, (const char*)deleteUser.c_str(), nullptr, nullptr, nullptr);

	string deleteAlbums = "DELETE FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + ";";
	sqlite3_exec(this->_db, (const char*)deleteAlbums.c_str(), nullptr, nullptr, nullptr);

	//getAlbums()
	string deleteTags = "DELETE FROM TAGS WHERE USER_ID = " + std::to_string(user.getId()) + ";";
	sqlite3_exec(this->_db, (const char*)deleteTags.c_str(), nullptr, nullptr, nullptr);
}

bool DatabaseAccess::doesUserExists(int userId)
{
	string query = "SELECT * FROM USERS WHERE ID = " + std::to_string(userId) + ";";
	std::list<User> users;

	int res = sqlite3_exec(this->_db, (const char*)query.c_str(), getUsersCallBack, (void*)&users, nullptr);

	if (res != SQLITE_OK) {
		throw std::runtime_error("Error in doesUserExists function");
	}

	return users.size() != 0;
}

User DatabaseAccess::getUser(int userId)
{
	string query = "SELECT * FROM USERS WHERE ID = " + std::to_string(userId) + ";";
	std::list<User> users;

	int res = sqlite3_exec(this->_db, (const char*)query.c_str(), getUsersCallBack, (void*)&users, nullptr);

	if (res != SQLITE_OK)
		throw std::runtime_error("Error in getUser function");

	if (users.size() == 0)
		throw std::runtime_error("The user in this id is not exist");

	std::list<User>::iterator iter = users.begin();

	return *iter;
	
}

int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	string query = "SELECT * FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + ";";

	std::list<Album> albums;

	int res = sqlite3_exec(this->_db, (const char*)query.c_str(), getAlbumsCallBack, (void*)&albums, nullptr);

	if (res != SQLITE_OK) {
		throw std::runtime_error("Error in getUser function");
	}

	return albums.size();
}

int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	string query = "SELECT COUNT(DISTINCT PICTURES.ALBUM_ID) FROM TAGS INNER JOIN PICTURES ON \
								TAGS.PICTURE_ID = PICTURES.ID WHERE USER_ID = "+ std::to_string(user.getId())+";";

	int countRes = 0;
	int res = sqlite3_exec(this->_db, (const char*)query.c_str(), getAmountCallBack, (void*)&countRes, nullptr);

	if (res != SQLITE_OK)
		throw std::runtime_error("Error in getUser function");

	return countRes;
}

int DatabaseAccess::countTagsOfUser(const User& user)
{
	string query = "SELECT COUNT(ID) FROM TAGS WHERE USER_ID = " + std::to_string(user.getId()) + ";";

	int countRes = 0;
	int res = sqlite3_exec(this->_db, (const char*)query.c_str(), getAmountCallBack, (void*)&countRes, nullptr);

	if (res != SQLITE_OK)
		throw std::runtime_error("Error in getUser function");

	return countRes;
}

float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	int totalAlbums = countAlbumsTaggedOfUser(user);
	int totalTags = countTagsOfUser(user);

	std::cout << totalTags << " " << totalAlbums << std::endl;
	if (totalAlbums == 0)
		return 0.0f;

	return totalTags / totalAlbums;
}

User DatabaseAccess::getTopTaggedUser()
{
	string query = "SELECT USER_ID, COUNT(USER_ID) AS value_occurrence FROM  TAGS " \
						 "GROUP BY USER_ID " \
							"ORDER BY value_occurrence DESC " \
							"LIMIT    1; ";

	int idTopTagged = -1;
	int resId = sqlite3_exec(_db, query.c_str(), getSomething, &idTopTagged, nullptr);

	if (resId != SQLITE_OK) 
		throw std::runtime_error("Error in getUser function");

	if (idTopTagged == -1) 
		throw std::runtime_error("There is no users tagged yet");

	return getUser(idTopTagged);
}

Picture DatabaseAccess::getTopTaggedPicture()
{
	const char* query = "SELECT PICTURE_ID, COUNT(PICTURE_ID) AS value_occurrence FROM  TAGS " \
		"GROUP BY PICTURE_ID " \
		"ORDER BY value_occurrence DESC " \
		"LIMIT 1; ";

	int idTopTagged = -1;
	int resId = sqlite3_exec(_db, query, getAmountCallBack, &idTopTagged, nullptr);

	if (idTopTagged == -1)
		throw std::runtime_error("There is no tags yet");

	if (resId != SQLITE_OK)
		throw std::runtime_error("Error in getUser function");

	std::list<Picture> pictures;
	string queryGetPic = "SELECT * FROM PICTURES WHERE ID = " + std::to_string(idTopTagged) + ";";
	int resPicture = sqlite3_exec(this->_db, queryGetPic.c_str(), getPicturesCallBack, (void*)&pictures, nullptr);
	
	return *pictures.begin();
}

std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
	if (doesUserExists(user.getId()))
	{
		string query = "SELECT PICTURES.ID, PICTURES.NAME, PICTURES.LOCATION, \
                       PICTURES.CREATION_DATE, PICTURES.ALBUM_ID FROM PICTURES INNER JOIN \
					TAGS ON PICTURES.ID = TAGS.PICTURE_ID WHERE TAGS.USER_ID = " + std::to_string(user.getId()) + ";";
		std::list<Picture> listPictures;
		sqlite3_exec(this->_db, query.c_str(), getPicturesCallBack, (void*)&listPictures, nullptr);
		
		std::list<Picture>::iterator iter;

		for (iter = listPictures.begin(); iter != listPictures.end(); iter++) {
			iter->setTagged(getTags(*iter));
		}
		return listPictures;
	}
	return std::list<Picture>();
}

