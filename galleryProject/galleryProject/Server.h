#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include "AlbumManager.h"
#include "DatabaseAccess.h"
#include "Helper.h"
#include "Constants.h"

class AlbumManager;
class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:
	SOCKET _serverSocket;
	void accept();
	void clientHandler(SOCKET clientSocket);

	AlbumManager* _albumManager;
	DatabaseAccess _da;
};


