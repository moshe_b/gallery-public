#include "Helper.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>

using std::string;

// recieves the type code of the message from socket (3 bytes)
// and returns the code. if no message found in the socket returns 0 (which means the client disconnected)
int Helper::getMessageTypeCode(SOCKET sc)
{
	char* s = getPartFromSocket(sc, 3);
	std::string msg(s);
	printf("in getMessageTypeCode");
	if (msg == "")
		return 0;

	int res = std::atoi(s);
	delete s;
	return  res;
}

// recieve data from socket according byteSize
// returns the data as int
int Helper::getIntPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	return atoi(s);
}

// recieve data from socket according byteSize
// returns the data as string
string Helper::getStringPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	string res(s);
	return res;
}

// return string after padding zeros if necessary
string Helper::getPaddedNumber(int num, int digits)
{
	std::ostringstream ostr;
	ostr << std::setw(digits) << std::setfill('0') << num;
	return ostr.str();

}

// recieve data from socket according byteSize
// this is private function
char* Helper::getPartFromSocket(SOCKET sc, int bytesNum)
{
	return getPartFromSocket(sc, bytesNum, 0);
}

char* Helper::getPartFromSocket(SOCKET sc, int bytesNum, int flags)
{
	if (bytesNum == 0)
	{
		return (char*)"";
	}

	char* data = new char[bytesNum + 1];
	int res = recv(sc, data, bytesNum, flags);

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	data[bytesNum] = 0;
	return data;
}

// send data to socket
// this is private function
void Helper::sendData(SOCKET sc, std::string message)
{
	const char* data = message.c_str();

	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}

void Helper::send_message_by_protocol(SOCKET sc, int code, std::vector<std::string> message_parts)
{
	std::string codeStr = getPaddedNumber(code, 3);

	std::string send_message = codeStr + "&";
	for (std::string message : message_parts) {
		send_message += message + "&";
	}

	send_message = send_message.substr(0, send_message.size() - 1);
	std::cout << send_message << std::endl;
	sendData(sc, send_message);
}

std::pair<CommandType, std::vector<std::string>> Helper::receivData(char* buf)
{
	std::pair<CommandType, std::vector<std::string>> message_from_client;
	std::string message = std::string(buf);

	printf("before type code \n");
	int code = std::stoi(message.substr(0, 3));
	message_from_client.first = static_cast<CommandType>(code);
	printf("code = %d \n", code);
	
	std::cout << buf << std::endl;
	if (message.size() > 3) //there is message that are only in length 3, for example 005
	{
		std::string data_from_client = message.substr(4, message.size());

		std::string delimiter = "&";
		size_t pos = 0;
		std::string token;
		while ((pos = data_from_client.find(delimiter)) != std::string::npos) {
			token = data_from_client.substr(0, pos);
			std::cout << token << "\n";
			message_from_client.second.push_back(token);
			data_from_client.erase(0, pos + delimiter.length());
		}

		message_from_client.second.push_back(data_from_client);
	}

	return message_from_client;
}
