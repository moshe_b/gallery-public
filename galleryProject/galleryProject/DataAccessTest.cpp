#include "DataAccessTest.h"

bool creatDB()
{
    DatabaseAccess db;
    return db.open();
}

void closeDB()
{
    DatabaseAccess db;
    db.close();
}

void deleteAlbum()
{
    DatabaseAccess db;
    db.deleteAlbum("first Name", 0);
}


void addPictureToAlbumByName()
{
    DatabaseAccess db;
    Picture pic(0, "picture ");
    db.addPictureToAlbumByName("album", pic);
}

void removePictureFromAlbumByName()
{
    DatabaseAccess db;
    db.removePictureFromAlbumByName("album", "picture ");
}

void tagUserInPicture()
{
    DatabaseAccess db;
    db.tagUserInPicture("alum", "piture ", 0);
}

void createUser()
{
    DatabaseAccess db;
    User user(3, "moshe");
    db.createUser(user);
}

void deleteUser()
{
    DatabaseAccess db;
    User user(3, "moshe");
    db.deleteUser(user);
}

void getAlbums()
{
    DatabaseAccess db;
    std::list<Album> albums;
    albums = db.getAlbums();
    std::list<Album>::iterator iter;

    for (iter = albums.begin(); iter != albums.end(); iter++)
    {
        std::cout << *iter << std::endl;
    }
}

void getAlbumsOfUser()
{
    DatabaseAccess db;
    User user(0, "");
    std::list<Album> albums =  db.getAlbumsOfUser(user);
    
    std::list<Album>::iterator iter;

    for (iter = albums.begin(); iter != albums.end(); iter++)
    {
        std::cout << *iter << std::endl;
    }
}

