﻿#include "AlbumManager.h"
#include <iostream>
#include "Constants.h"
#include "MyException.h"
#include "AlbumNotOpenException.h"

#pragma warning (disable: 4996)
AlbumManager::AlbumManager(IDataAccess& dataAccess) :
    m_dataAccess(dataAccess), m_nextPictureId(100), m_nextUserId(200)
{
	m_dataAccess.open();
}

std::pair<CommandType, std::vector<std::string>>  AlbumManager::executeCommand(CommandType command, std::vector<std::string> data) {
	try {
		AlbumManager::handler_func_t handler = m_commands.at(command);
		return (this->*handler)(data);
	} catch (const std::out_of_range&) {
			throw MyException("Error: Invalid command[" + std::to_string(command) + "]\n");
	}

	return std::pair<CommandType, std::vector<std::string>>();
}

std::string AlbumManager::printHelp() const
{
	std::cout << "Supported Album commands:" << std::endl;
	std::cout << "*************************" << std::endl;
	
	for (const struct CommandGroup& group : m_prompts) {
		std::cout << group.title << std::endl;
		std::string space(".  ");
		for (const struct CommandPrompt& command : group.commands) {
			space = command.type < 10 ? ".   " : ".  ";

			std::cout << command.type << space << command.prompt << std::endl;
		}
		std::cout << std::endl;
	}

	return "";
}


// ******************* Album ******************* 
std::pair<CommandType, std::vector<std::string>>  AlbumManager::createAlbum(std::vector<std::string> data)
{
	std::string userIdStr = data[0];
	std::vector<std::string> all_albums;
	int userId = std::stoi(userIdStr);
	if ( !m_dataAccess.doesUserExists(userId) ) {
		all_albums.push_back("ERROR");
		all_albums.push_back("Error: Can't create album since there is no user with id [" + userIdStr+"]");
		return std::make_pair(CommandType::CREATE_ALBUM, all_albums);
	}

	std::string name = data[1];
	if ( m_dataAccess.doesAlbumExists(name,userId) ) {
		all_albums.push_back("ERROR");
		all_albums.push_back("Error: Failed to create album, album with the same name already exists");
		return std::make_pair(CommandType::CREATE_ALBUM, all_albums);
	}

	Album newAlbum(userId,name);
	m_dataAccess.createAlbum(newAlbum);

	std::cout << "Album [" << newAlbum.getName() << "] created successfully by user@" << newAlbum.getOwnerId() << std::endl;
	
	//return the list of albums for refresh in the front end
	all_albums = listAlbums(std::vector<std::string>()).second;
	std::pair<CommandType, std::vector<std::string>> retVal = std::make_pair(CommandType::CREATE_ALBUM, all_albums);
	return retVal;
}

std::pair<CommandType, std::vector<std::string>>  AlbumManager::openAlbum(std::vector<std::string> data)
{
	if (isCurrentAlbumSet()) {
		//need to add the album Name
		closeAlbum(std::vector<std::string>());
	}

	std::string userIdStr = data[0];
	int userId = std::stoi(userIdStr);
	if ( !m_dataAccess.doesUserExists(userId) ) {
		throw MyException("Error: Can't open album since there is no user with id @" + userIdStr + ".\n");
	}

	std::string name = data[1];
	if ( !m_dataAccess.doesAlbumExists(name, userId) ) {
		throw MyException("Error: Failed to open album, since there is no album with name:"+name +".\n");
	}

	m_openAlbum = m_dataAccess.openAlbum(name);
    m_currentAlbumName = name;
	// success
	std::cout << "Album [" << name << "] opened successfully." << std::endl;
	return std::pair<CommandType, std::vector<std::string>>(); //dont return anything because it's a server process
}

std::pair<CommandType, std::vector<std::string>> AlbumManager::closeAlbum(std::vector<std::string> data)
{
	refreshOpenAlbum();

	std::cout << "Album [" << m_openAlbum.getName() << "] closed successfully." << std::endl;
	m_dataAccess.closeAlbum(m_openAlbum);
	m_currentAlbumName = "";

	return std::pair<CommandType, std::vector<std::string>>(); //server process, front end don't need anything
}

std::pair<CommandType, std::vector<std::string>> AlbumManager::deleteAlbum(std::vector<std::string> data)
{
	std::string userIdStr = data[0];
	int userId = std::stoi(userIdStr);
	if (!m_dataAccess.doesUserExists(userId)) {
		throw MyException("Error: There is no user with id @" + userIdStr +"\n");
	}

	std::string albumName = data[1];
	if ( !m_dataAccess.doesAlbumExists(albumName, userId) ) {
		throw MyException("Error: Failed to delete album, since there is no album with name:" + albumName + ".\n");
	}

	// album exist, close album if it is opened
	if ( (isCurrentAlbumSet() ) &&
		 (m_openAlbum.getOwnerId() == userId && m_openAlbum.getName() == albumName) ) {

		//need to add the album name to the vector
		closeAlbum(std::vector<std::string>());
	}

	m_dataAccess.deleteAlbum(albumName, userId);
	std::cout << "Album [" << albumName << "] @"<< userId <<" deleted successfully." << std::endl;

	//return the list of albums for refresh in the front end
	std::vector<std::string> all_albums = listAlbums(std::vector<std::string>()).second;
	std::pair<CommandType, std::vector<std::string>> retVal = std::make_pair(CommandType::DELETE_ALBUM, all_albums);
	return retVal;
}

std::pair<CommandType, std::vector<std::string>>  AlbumManager::listAlbums(std::vector<std::string> data)
{
	return std::make_pair<CommandType, std::vector<string>>(CommandType::LIST_ALBUMS, m_dataAccess.printAlbums());
}

std::pair<CommandType, std::vector<std::string>> AlbumManager::listAlbumsOfUser(std::vector<std::string> data)
{
	std::string userIdStr = data[0];
	int userId = std::stoi(userIdStr);
	if (!m_dataAccess.doesUserExists(userId)) {
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	}

	const User& user = m_dataAccess.getUser(userId);
	const std::list<Album>& albums = m_dataAccess.getAlbumsOfUser(user);

	std::cout << "Albums list of user@" << user.getId() << ":" << std::endl;
	std::cout << "-----------------------" << std::endl;

	std::vector<std::string> albumsUser;
	for (const auto& album : albums) {
		albumsUser.push_back("   + [" + album.getName() + "] - created on " + album.getCreationDate());
	}

	std::pair<CommandType, std::vector<std::string>> retVal = std::make_pair(CommandType::LIST_ALBUMS_OF_USER, albumsUser);
	return retVal;
}


// ******************* Picture ******************* 
std::pair<CommandType, std::vector<std::string>> AlbumManager::addPictureToAlbum(std::vector<std::string> data)
{
	refreshOpenAlbum();

	std::string picName = data[0];
	if (m_openAlbum.doesPictureExists(picName) ) {
		throw MyException("Error: Failed to add picture, picture with the same name already exists.\n");
	}
	
	Picture picture(++m_nextPictureId, picName);
	std::string picPath = data[1];
	picture.setPath(picPath);

	m_dataAccess.addPictureToAlbumByName(m_openAlbum.getName(), picture);

	std::cout << "Picture [" << picture.getId() << "] successfully added to Album [" << m_openAlbum.getName() << "]." << std::endl;
	
	std::vector<std::string> albums;
	albums.push_back(m_openAlbum.getName());
	std::vector<std::string> list_pictures_in_album = listPicturesInAlbum(albums).second;
	std::pair<CommandType, std::vector<std::string>> retVal = std::make_pair(CommandType::ADD_PICTURE, list_pictures_in_album);
	return retVal;
}

std::pair<CommandType, std::vector<std::string>>  AlbumManager::removePictureFromAlbum(std::vector<std::string> data)
{
	refreshOpenAlbum();

	std::string picName = data[0];
	if ( !m_openAlbum.doesPictureExists(picName) ) {
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	}
	
	auto picture = m_openAlbum.getPicture(picName);
	m_dataAccess.removePictureFromAlbumByName(m_openAlbum.getName(), picture.getName());
	std::cout << "Picture <" << picName << "> successfully removed from Album [" << m_openAlbum.getName() << "]." << std::endl;
	
	std::vector<std::string> albums;
	albums.push_back(m_openAlbum.getName());
	std::vector<std::string> list_pictures_in_album = listPicturesInAlbum(albums).second;
	std::pair<CommandType, std::vector<std::string>> retVal = std::make_pair(CommandType::REMOVE_PICTURE, list_pictures_in_album);
	return retVal;
}

std::pair<CommandType, std::vector<std::string>>  AlbumManager::listPicturesInAlbum(std::vector<std::string> data)
{
	refreshOpenAlbum();

	std::cout << "List of pictures in Album [" << m_openAlbum.getName() 
			  << "] of user@" << m_openAlbum.getOwnerId() <<":" << std::endl;
	
	std::vector<std::string> albumsRes;
	const std::list<Picture>& albumPictures = m_openAlbum.getPictures();
	for (auto iter = albumPictures.begin(); iter != albumPictures.end(); ++iter) {
		albumsRes.push_back("   + Picture [" + std::to_string(iter->getId()) + "] - " + iter->getName() + 
			"\tLocation: [" + iter->getPath() + "]\tCreation Date: [" +
				iter->getCreationDate() + "]\tTags: [" + std::to_string(iter->getTagsCount()) + "]");
	}
	std::cout << std::endl;

	std::pair<CommandType, std::vector<std::string>> retVal = std::make_pair(CommandType::LIST_PICTURES, albumsRes);
	return retVal;
}

std::pair<CommandType, std::vector<std::string>>  AlbumManager::showPicture(std::vector<std::string> data)
{
	refreshOpenAlbum();

	std::string picName = getInputFromConsole("Enter picture name: ");
	if ( !m_openAlbum.doesPictureExists(picName) ) {
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	}
	
	auto pic = m_openAlbum.getPicture(picName);
	if ( !fileExistsOnDisk(pic.getPath()) ) {
		throw MyException("Error: Can't open <" + picName+ "> since it doesnt exist on disk.\n");
	}

	// Bad practice!!!
	// Can lead to privileges escalation
	// You will replace it on WinApi Lab(bonus)
	system(pic.getPath().c_str()); 

	return std::pair<CommandType, std::vector<std::string>>();
}

std::pair<CommandType, std::vector<std::string>>  AlbumManager::tagUserInPicture(std::vector<std::string> data)
{
	refreshOpenAlbum();

	std::cout << "data[0] = " << data[0] << " data[1]" << data[1];
	std::string picName = data[0];
	if (!m_openAlbum.doesPictureExists(picName)) {
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	}

	Picture pic = m_openAlbum.getPicture(picName);
	std::vector<std::string> userTagged; //show it in the front end for knowing that is a sucess operation

	std::string userIdStr = data[1];
	int userId = std::stoi(userIdStr);
	if (!m_dataAccess.doesUserExists(userId)) {
		userTagged.push_back("ERROR");
		return std::make_pair(CommandType::TAG_USER, userTagged);
		//throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	}
	User user = m_dataAccess.getUser(userId);

	m_dataAccess.tagUserInPicture(m_openAlbum.getName(), pic.getName(), user.getId());
	std::cout << "User @" << userIdStr << " successfully tagged in picture <" << pic.getName() << "> in album [" << m_openAlbum.getName() << "]" << std::endl;

	userTagged.push_back("User @" + userIdStr + " successfully tagged in picture <" + pic.getName() + "> in album [" + m_openAlbum.getName() + "]");
	std::pair<CommandType, std::vector<std::string>> retVal = std::make_pair(CommandType::TAG_USER, userTagged);
	return retVal;
}

std::pair<CommandType, std::vector<std::string>>  AlbumManager::untagUserInPicture(std::vector<std::string> data)
{
	refreshOpenAlbum();

	std::vector<std::string> userUnTagged; //show it in the front end for knowing that is a sucess operation
	std::string picName = data[0];
	if (!m_openAlbum.doesPictureExists(picName)) {
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	}

	Picture pic = m_openAlbum.getPicture(picName);

	std::string userIdStr = data[1];
	int userId = stoi(userIdStr);
	if (!m_dataAccess.doesUserExists(userId)) {
		userUnTagged.push_back("ERROR");
		userUnTagged.push_back("Error: There is no user with id @" + userIdStr + "\n");
		return std::make_pair(CommandType::UNTAG_USER, userUnTagged);
	}
	User user = m_dataAccess.getUser(userId);

	if (! pic.isUserTagged(user)) {
		userUnTagged.push_back("ERROR");
		userUnTagged.push_back("Error: The user was not tagged! \n");
		return std::make_pair(CommandType::UNTAG_USER, userUnTagged);
	}

	m_dataAccess.untagUserInPicture(m_openAlbum.getName(), pic.getName(), user.getId());
	std::cout << "User @" << userIdStr << " successfully untagged in picture <" << pic.getName() << "> in album [" << m_openAlbum.getName() << "]" << std::endl;

	userUnTagged.push_back("User @" + userIdStr + " successfully untagged in picture <" + pic.getName() + "> in album [" + m_openAlbum.getName() + "]");
	std::pair<CommandType, std::vector<std::string>> retVal = std::make_pair(CommandType::UNTAG_USER, userUnTagged);
	return retVal;
}

std::pair<CommandType, std::vector<std::string>>  AlbumManager::listUserTags(std::vector<std::string> data)
{
	refreshOpenAlbum();

	std::string picName = data[0];
	if ( !m_openAlbum.doesPictureExists(picName) ) {
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	}
	auto pic = m_openAlbum.getPicture(picName); 

	const std::set<int> users = pic.getUserTags();
	std::vector<std::string> listTagged;

	if ( 0 == users.size() )  {
		listTagged.push_back("ERROR");
		return  std::make_pair(CommandType::LIST_TAGS, listTagged);
	}

	std::cout << "Tagged users in picture <" << picName << ">:" << std::endl;
	for (const int user_id: users) {
		const User user = m_dataAccess.getUser(user_id);
		std::ostringstream oss;
		oss << user;
		listTagged.push_back(oss.str());
		std::cout << user << std::endl;
	}
	std::cout << std::endl;

	std::pair<CommandType, std::vector<std::string>> retVal = std::make_pair(CommandType::LIST_TAGS, listTagged);
	return retVal;
}


// ******************* User ******************* 
std::pair<CommandType, std::vector<std::string>>  AlbumManager::addUser(std::vector<std::string> data)
{
	std::string name = data[0];

	User user(++m_nextUserId,name);
	
	m_dataAccess.createUser(user);
	std::vector<std::string> list_users;
	//create_user.push_back("User " + name + " with id @" + std::to_string(user.getId()) + " created successfully.");
	std::cout << "User " << name << " with id @" << user.getId() << " created successfully." << std::endl;

	list_users = listUsers(data).second;
	std::pair<CommandType, std::vector<std::string>> retVal = std::make_pair(CommandType::ADD_USER, list_users);
	return retVal;
}


std::pair<CommandType, std::vector<std::string>> AlbumManager::removeUser(std::vector<std::string> data)
{
	// get user name
	std::string userIdStr = data[0];
	int userId = std::stoi(userIdStr);
	if ( !m_dataAccess.doesUserExists(userId) ) {
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	}
	const User& user = m_dataAccess.getUser(userId);
	if (isCurrentAlbumSet() && userId == m_openAlbum.getOwnerId()) {
		closeAlbum(std::vector<std::string>());
	}

	m_dataAccess.deleteUser(user);
	std::cout << "User @" << userId << " deleted successfully." << std::endl;

	std::vector<std::string> users = listUsers(data).second;
	std::pair<CommandType, std::vector<std::string>> retVal = std::make_pair(CommandType::REMOVE_USER, users);
	return retVal;
}

std::pair<CommandType, std::vector<std::string>> AlbumManager::listUsers(std::vector<std::string> data)
{
	std::pair<CommandType, std::vector<std::string>> retVal = std::make_pair(CommandType::LIST_OF_USER, m_dataAccess.printUsers());
	return retVal;
}

std::pair<CommandType, std::vector<std::string>>  AlbumManager::userStatistics(std::vector<std::string> data)
{
	std::string userIdStr = data[0];
	int userId = std::stoi(userIdStr);
	std::vector<std::string> user_statistics;
	if ( !m_dataAccess.doesUserExists(userId) ) {
		user_statistics.push_back("ERROR");
		user_statistics.push_back("Error: There is no user with id @" + userIdStr + "\n");
		return std::make_pair(CommandType::USER_STATISTICS, user_statistics);
	}

	const User& user = m_dataAccess.getUser(userId);

	user_statistics.push_back("user @" + userIdStr + " Statistics: --------------------\n" 
		"  + Count of Albums Tagged: " + std::to_string(m_dataAccess.countAlbumsTaggedOfUser(user)) + "\n" + 
		"  + Count of Tags: " + std::to_string(m_dataAccess.countTagsOfUser(user)) + "\n" + 
		"  + Avarage Tags per Alboum: " + std::to_string(m_dataAccess.averageTagsPerAlbumOfUser(user)) + "\n");
	std::pair<CommandType, std::vector<std::string>> retVal = std::make_pair(CommandType::USER_STATISTICS, user_statistics);
	return retVal;
}


// ******************* Queries ******************* 
std::pair<CommandType, std::vector<std::string>>  AlbumManager::topTaggedUser(std::vector<std::string> data)
{
	const User& user = m_dataAccess.getTopTaggedUser();

	std::vector<std::string> userTagged;
	userTagged.push_back("The top tagged user is: " + user.getName() + "\n");

	std::pair<CommandType, std::vector<std::string>> retVal = std::make_pair(CommandType::TOP_TAGGED_USER, userTagged);
	return retVal;
}

std::pair<CommandType, std::vector<std::string>>  AlbumManager::topTaggedPicture(std::vector<std::string> data)
{
	const Picture& picture = m_dataAccess.getTopTaggedPicture();

	std::cout << "The top tagged picture is: " << picture.getName() << std::endl;

	std::vector<std::string> picTagged;
	picTagged.push_back("The top tagged picture is: " + picture.getName() + "\n");
	std::pair<CommandType, std::vector<std::string>> retVal = std::make_pair(CommandType::TOP_TAGGED_PICTURE, picTagged);
	return retVal;
}

std::pair<CommandType, std::vector<std::string>> AlbumManager::picturesTaggedUser(std::vector<std::string> data)
{
	std::string userIdStr = data[0];
	int userId = std::stoi(userIdStr);
	std::vector<std::string> pictures_tagged_user;
	if ( !m_dataAccess.doesUserExists(userId) ) {
		pictures_tagged_user.push_back("ERROR");
		pictures_tagged_user.push_back("Error: There is no user with id @" + userIdStr);
		return std::make_pair(CommandType::PICTURES_TAGGED_USER, pictures_tagged_user);
	}

	auto user = m_dataAccess.getUser(userId);

	auto taggedPictures = m_dataAccess.getTaggedPicturesOfUser(user);

	//std::cout << "List of pictures that User@" << user.getId() << " tagged :" << std::endl;
	
	for (const Picture& picture: taggedPictures) {
		std::ostringstream oss;
		oss <<"   + "<< picture << std::endl;
		pictures_tagged_user.push_back(oss.str());
	}
	std::cout << std::endl;

	std::pair<CommandType, std::vector<std::string>> retVal = 
				std::make_pair(CommandType::PICTURES_TAGGED_USER, pictures_tagged_user);
	return retVal;
}


// ******************* Help & exit ******************* 
std::pair<CommandType, std::vector<std::string>>  AlbumManager::exit(std::vector<std::string> data)
{
	std::exit(EXIT_SUCCESS);

	return std::pair<CommandType, std::vector<std::string>>();
}

std::pair<CommandType, std::vector<std::string>>  AlbumManager::help(std::vector<std::string> data)
{
	system("CLS");
	printHelp();

	return std::pair<CommandType, std::vector<std::string>>();
}

std::string AlbumManager::getInputFromConsole(const std::string& message)
{
	std::string input;
	do {
		std::cout << message;
		std::getline(std::cin, input);
	} while (input.empty());
	
	return input;
}

bool AlbumManager::fileExistsOnDisk(const std::string& filename)
{
	struct stat buffer;   
	return (stat(filename.c_str(), &buffer) == 0); 
}

void AlbumManager::refreshOpenAlbum() {
	if (!isCurrentAlbumSet()) {
		throw AlbumNotOpenException();
	}
    m_openAlbum = m_dataAccess.openAlbum(m_currentAlbumName);
}

bool AlbumManager::isCurrentAlbumSet() const
{
    return !m_currentAlbumName.empty();
}

std::pair<CommandType, std::vector<std::string>> AlbumManager::change_img_read(std::vector<std::string> data)
{
	std::string image_path = data[0];
	std::cout << "IMAGE PATH = " << image_path << std::endl;
	WIN32_FIND_DATA FileData;
	HANDLE          hSearch;
	DWORD           dwAttrs;

	wchar_t wtext[100] = {0};
	mbstowcs(wtext, image_path.c_str(), image_path.size()+1);

	FindFirstFile(wtext, &FileData);
	dwAttrs = GetFileAttributes(FileData.cFileName);

	printf("%d", SetFileAttributes(wtext,
		dwAttrs | FILE_ATTRIBUTE_READONLY));
	std::pair<CommandType, std::vector<std::string>> retVal =
		std::make_pair(CommandType::READ_ONLY_PICTURE, data);
	return retVal;
}

std::pair<CommandType, std::vector<std::string>> AlbumManager::change_img_read_write(std::vector<std::string> data)
{
	std::string image_path = data[0];
	std::cout << "IMAGE PATH = " << image_path << std::endl;
	WIN32_FIND_DATA FileData;
	HANDLE          hSearch;
	DWORD           dwAttrs;

	wchar_t wtext[100] = { 0 };
	mbstowcs(wtext, image_path.c_str(), image_path.size() + 1);

	FindFirstFile(wtext, &FileData);
	dwAttrs = GetFileAttributes(FileData.cFileName);

	printf("%d", SetFileAttributes(wtext,
		FILE_ATTRIBUTE_NORMAL));
	std::pair<CommandType, std::vector<std::string>> retVal =
		std::make_pair(CommandType::READ_WRITE_PICTURE, data);
	return retVal;
}

std::pair<CommandType, std::vector<std::string>> AlbumManager::open_with_paint(std::vector<std::string> data)
{
	std::string image_path = data[0];
	std::cout << "IMAGE PATH = " << image_path << std::endl;

	wchar_t wtext[100] = { 0 };
	mbstowcs(wtext, image_path.c_str(), image_path.size() + 1);

	ShellExecute(nullptr, TEXT("open"),
		TEXT("mspaint.exe"),
		wtext, nullptr, SW_SHOWMAXIMIZED);
	
	std::pair<CommandType, std::vector<std::string>> retVal =
		std::make_pair(CommandType::OPEN_WITH_PAINT, data);
	return retVal;
}

const std::vector<struct CommandGroup> AlbumManager::m_prompts  = {
	{
		"Supported Albums Operations:\n----------------------------",
		{
			{ CREATE_ALBUM        , "Create album" },
			{ OPEN_ALBUM          , "Open album" },
			{ CLOSE_ALBUM         , "Close album" },
			{ DELETE_ALBUM        , "Delete album" },
			{ LIST_ALBUMS         , "List albums" },
			{ LIST_ALBUMS_OF_USER , "List albums of user" }
		}
	},
	{
		"Supported Album commands (when specific album is open):",
		{
			{ ADD_PICTURE    , "Add picture." },
			{ REMOVE_PICTURE , "Remove picture." },
			{ SHOW_PICTURE   , "Show picture." },
			{ LIST_PICTURES  , "List pictures." },
			{ TAG_USER		 , "Tag user." },
			{ UNTAG_USER	 , "Untag user." },
			{ LIST_TAGS		 , "List tags." }
		}
	},
	{
		"Supported Users commands: ",
		{
			{ ADD_USER         , "Add user." },
			{ REMOVE_USER      , "Remove user." },
			{ LIST_OF_USER     , "List of users." },
			{ USER_STATISTICS  , "User statistics." },
		}
	},
	{
		"Supported Queries:",
		{
			{ TOP_TAGGED_USER      , "Top tagged user." },
			{ TOP_TAGGED_PICTURE   , "Top tagged picture." },
			{ PICTURES_TAGGED_USER , "Pictures tagged user." },
		}
	},
	{
		"Change Permmisions:",
		{
			{READ_ONLY_PICTURE , "Read Only Picture."},
			{READ_WRITE_PICTURE, "Read And Write."}
		}
	},
	{
		"Supported Operations:",
		{
			{ HELP , "Help (clean screen)" },
			{ EXIT , "Exit." },
		}
	}
};

const std::map<CommandType, AlbumManager::handler_func_t> AlbumManager::m_commands = {
	{ CREATE_ALBUM, &AlbumManager::createAlbum },
	{ OPEN_ALBUM, &AlbumManager::openAlbum },
	{ CLOSE_ALBUM, &AlbumManager::closeAlbum },
	{ DELETE_ALBUM, &AlbumManager::deleteAlbum },
	{ LIST_ALBUMS, &AlbumManager::listAlbums },
	{ LIST_ALBUMS_OF_USER, &AlbumManager::listAlbumsOfUser },
	{ ADD_PICTURE, &AlbumManager::addPictureToAlbum },
	{ REMOVE_PICTURE, &AlbumManager::removePictureFromAlbum },
	{ LIST_PICTURES, &AlbumManager::listPicturesInAlbum },
	{ SHOW_PICTURE, &AlbumManager::showPicture },
	{ TAG_USER, &AlbumManager::tagUserInPicture, },
	{ UNTAG_USER, &AlbumManager::untagUserInPicture },
	{ LIST_TAGS, &AlbumManager::listUserTags },
	{ ADD_USER, &AlbumManager::addUser },
	{ REMOVE_USER, &AlbumManager::removeUser },
	{ LIST_OF_USER, &AlbumManager::listUsers },
	{ USER_STATISTICS, &AlbumManager::userStatistics },
	{ TOP_TAGGED_USER, &AlbumManager::topTaggedUser },
	{ TOP_TAGGED_PICTURE, &AlbumManager::topTaggedPicture },
	{ PICTURES_TAGGED_USER, &AlbumManager::picturesTaggedUser },
	{ READ_ONLY_PICTURE, &AlbumManager::change_img_read},
	{ READ_WRITE_PICTURE, &AlbumManager::change_img_read_write},
	{ OPEN_WITH_PAINT, &AlbumManager::open_with_paint},
	{ HELP, &AlbumManager::help },
	{ EXIT, &AlbumManager::exit }
};
