#pragma once
#include "IDataAccess.h"
#include <io.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include "sqlite3.h"
using std::string;
int getAlbumsCallBack(void* data, int argc, char** argv, char** azColName);
int getUsersCallBack(void* data, int argc, char** argv, char** azColName);
int getAmountCallBack(void* data, int argc, char** argv, char** azColName);
int getPictureTagsCallBack(void* data, int argc, char** argv, char** azColName);
int getPicturesCallBack(void* data, int argc, char** argv, char** azColName);
int getTagsPicCallBack(void* data, int argc, char** argv, char** azColName);
int getSomething(void* data, int argc, char** argv, char** azColName);
typedef enum {USERS, ALBUMS, TAGS, PICTURES} kind_table;


class DatabaseAccess :
    public IDataAccess
{
public:
	DatabaseAccess();

	const std::list<Album> getAlbums() override;
	const std::list<Album> getAlbumsOfUser(const User& user) override;
	void createAlbum(const Album& album) override;
	void deleteAlbum(const std::string& albumName, int userId) override;
	bool doesAlbumExists(const std::string& albumName, int userId) override;
	Album openAlbum(const std::string& albumName) override;
	void closeAlbum(Album& pAlbum) override;
	std::vector<std::string> printAlbums() override;

	// picture related
	void addPictureToAlbumByName(const std::string& albumName, const Picture& picture) override;
	void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName) override;
	void tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;
	void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;

	// user related
	std::vector<std::string> printUsers() override;
	void createUser(User& user) override;
	void deleteUser(const User& user) override;
	bool doesUserExists(int userId) override;
	User getUser(int userId) override;

	// user statistics
	int countAlbumsOwnedOfUser(const User& user) override;
	int countAlbumsTaggedOfUser(const User& user) override;
	int countTagsOfUser(const User& user) override;
	float averageTagsPerAlbumOfUser(const User& user) override;

	// queries
	User getTopTaggedUser() override;
	Picture getTopTaggedPicture() override;
	std::list<Picture> getTaggedPicturesOfUser(const User& user) override;

	bool open() override;
	void close() override;
	void clear() override;

	//get maximum id
	int getMaxId(kind_table kind);
private:
	std::list<Album> m_albums;
	std::list<User> m_users;
	sqlite3* _db;
	std::list<Picture>* getPictures(std::string albumName);
	//delete tags
	void deleteTags(std::list<std::pair<int, int>> tagsPicIds);
	void deletePictures(std::list<std::pair<int, int>> tagsPicIds);
	std::set<int> getTags(Picture pic);
};
