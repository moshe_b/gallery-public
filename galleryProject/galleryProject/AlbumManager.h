﻿#pragma once
#include <vector>
#include "Constants.h"
#include "MemoryAccess.h"
#include "Album.h"
#include "Server.h"
#include <Windows.h>

class IDataAccess;
class AlbumManager
{
public:
	AlbumManager(IDataAccess& dataAccess);
	std::pair<CommandType, std::vector<std::string>> executeCommand(CommandType command, std::vector<std::string>);
	std::string  printHelp() const;

	using handler_func_t = std::pair<CommandType, std::vector<std::string>> (AlbumManager::*)(std::vector<std::string>);

private:
    int m_nextPictureId{};
    int m_nextUserId{};
    std::string m_currentAlbumName{};
	IDataAccess& m_dataAccess;
	Album m_openAlbum;

	std::pair<CommandType, std::vector<std::string>> help(std::vector<std::string> data);
	// albums management
	std::pair<CommandType, std::vector<std::string>> createAlbum(std::vector<std::string>);
	std::pair<CommandType, std::vector<std::string>> openAlbum(std::vector<std::string>);
	std::pair<CommandType, std::vector<std::string>> closeAlbum(std::vector<std::string>);
	std::pair<CommandType, std::vector<std::string>>  deleteAlbum(std::vector<std::string>);
	std::pair<CommandType, std::vector<std::string>>  listAlbums(std::vector<std::string>);
	std::pair<CommandType, std::vector<std::string>> listAlbumsOfUser(std::vector<std::string>);

	// Picture management
	std::pair<CommandType, std::vector<std::string>> addPictureToAlbum(std::vector<std::string>);
	std::pair<CommandType, std::vector<std::string>> removePictureFromAlbum(std::vector<std::string>);
	std::pair<CommandType, std::vector<std::string>> listPicturesInAlbum(std::vector<std::string>);
	std::pair<CommandType, std::vector<std::string>>  showPicture(std::vector<std::string>);

	// tags related
	std::pair<CommandType, std::vector<std::string>>  tagUserInPicture(std::vector<std::string>);
	std::pair<CommandType, std::vector<std::string>> untagUserInPicture(std::vector<std::string>);
	std::pair<CommandType, std::vector<std::string>> listUserTags(std::vector<std::string>);

	// users management
	std::pair<CommandType, std::vector<std::string>> addUser(std::vector<std::string>);
	std::pair<CommandType, std::vector<std::string>> removeUser(std::vector<std::string>);
	std::pair<CommandType, std::vector<std::string>> listUsers(std::vector<std::string>);
	std::pair<CommandType, std::vector<std::string>> userStatistics(std::vector<std::string>);

	std::pair<CommandType, std::vector<std::string>> topTaggedUser(std::vector<std::string>);
	std::pair<CommandType, std::vector<std::string>> topTaggedPicture(std::vector<std::string>);
	std::pair<CommandType, std::vector<std::string>>  picturesTaggedUser(std::vector<std::string>);
	std::pair<CommandType, std::vector<std::string>>  exit(std::vector<std::string> data);

	std::string getInputFromConsole(const std::string& message);
	bool fileExistsOnDisk(const std::string& filename);
	void refreshOpenAlbum();
    bool isCurrentAlbumSet() const;

	static const std::vector<struct CommandGroup> m_prompts;
	static const std::map<CommandType, handler_func_t> m_commands;

	std::pair<CommandType, std::vector<std::string>>  change_img_read(std::vector<std::string> data);
	std::pair<CommandType, std::vector<std::string>>  change_img_read_write(std::vector<std::string> data);

	std::pair<CommandType, std::vector<std::string>>  open_with_paint(std::vector<std::string> data);
};

