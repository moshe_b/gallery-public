from tkinter import *
from PIL import Image, ImageTk
import Helper
from statistics import show_statistics_win
from tkinter import messagebox

class GetStatistics(object):
    def __init__(self, parent, client):
        self._win = Toplevel()
        self._parent_win = parent
        self._sock = client
        self._win.title('Get Statistics')
        self._win.geometry("250x200")
        self._win.iconphoto(False, PhotoImage(file='Assets/gallery_logo.png'))
        self._user_icon = ImageTk.PhotoImage(Image.open('Assets/user_icon.png').resize((50, 50)))
        self._user_id_label = Label(self._win, text='User Id: ')
        self._user_id_label.grid(row=0, column=0)
        self._user_id_entry = Entry(self._win, width=25)
        self._user_id_entry.grid(row=0, column=1)
        self._button_create = Button(self._win, text='Get+', width=20, command=self.get_statistics).grid(row=1, column=0, columnspan=2, padx=10, pady=10)
        self._label_user = Label(self._win)
        self._label_user.image = self._user_icon
        self._label_user.config(image=self._user_icon)
        self._label_user.grid(row=2, column=0)
        self._win.mainloop()

    def get_statistics(self):
        user_id = self._user_id_entry.get()
        data = [user_id]
        print(data)
        if user_id.isdigit():
            code, statistics = self._sock.recv_and_send(17, data)
            if statistics[0] == "ERROR":
                messagebox.showerror(title='ERROR', message=statistics[1])
            else:
                print(statistics)
                data = list(Helper.parse_statistics_data(statistics))
                show_statistics_win(data)
                self._parent_win.deiconify()
                self._win.destroy()
        else:
            messagebox.showerror('ERROR', 'You must enter a number')
