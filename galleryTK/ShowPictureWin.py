from tkinter import *
from PIL import Image, ImageTk
from tkinter import messagebox
import openCvManagment


class ShowPicture(object):
    def __init__(self, client, picture_data):
        self._win = Toplevel()
        self._win.geometry("500x500")
        self._picture_data = picture_data
        self._client = client
        self._image_pic = None
        self._image_path = ""
        try:
            self._image_path = picture_data[2]
            self._image_pic = ImageTk.PhotoImage(Image.open(picture_data[2]).resize((200, 200)))
        except Exception:
            self._image_path = 'Assets/image_icon.png'
            self._image_pic = ImageTk.PhotoImage(Image.open('Assets/image_icon.png'))

        # radio buttons variable
        self._var = IntVar()

        # radio buttons options
        self._gray_img = Radiobutton(self._win,
                              text='Gray color image',
                              font=("Impact", 10),
                              compound='left',
                              variable=self._var,
                              value=1)
        self._gray_img.place(x=20, y=30)

        self._half_top = Radiobutton(self._win,
                              text='half top image',
                              font=("Impact", 10),
                              compound='left',
                              variable=self._var,
                              value=2)
        self._half_top.place(x=20, y=50)

        self._half_low = Radiobutton(self._win,
                              text='half low image',
                              font=("Impact", 10),
                              compound='left',
                              variable=self._var,
                              value=3)
        self._half_low.place(x=20, y=70)

        self._half_left = Radiobutton(self._win,
                              text='half left image',
                              font=("Impact", 10),
                              compound='left',
                              variable=self._var,
                              value=4)
        self._half_left.place(x=20, y=90)

        self._half_right = Radiobutton(self._win,
                              text='half right image',
                              font=("Impact", 10),
                              compound='left',
                              variable=self._var,
                              value=5)
        self._half_right.place(x=20, y=110)

        self._open_with_paint = Radiobutton(self._win,
                              text='Open with Paint',
                              font=("Impact", 10),
                              compound='left',
                              variable=self._var,
                              value=6)
        self._open_with_paint.place(x=20, y=130)

        self._draw_advanced = Radiobutton(self._win,
                              text='Draw Advanced',
                              font=("Impact", 10),
                              compound='left',
                              variable=self._var,
                              value=7)
        self._draw_advanced.place(x=20, y=150)

        self._draw_circles = Radiobutton(self._win,
                              text='Draw Circles',
                              font=("Impact", 10),
                              compound='left',
                              variable=self._var,
                              value=8)
        self._draw_circles.place(x=20, y=170)

        self._draw_circles = Radiobutton(self._win,
                              text='hsv detection',
                              font=("Impact", 10),
                              compound='left',
                              variable=self._var,
                              value=9)
        self._draw_circles.place(x=20, y=190)

        # image
        self._picture = Label(self._win, borderwidth=3, image=self._image_pic, width=200, height=200)
        self._picture.image = self._image_pic
        self._picture.place(x=200, y=20)

        self._edit_image = Button(self._win,width=20, text="Edit", command=self.show_pic)
        self._edit_image.place(x=240, y=250)

    def show_pic(self):
        val_radio_button = self._var.get()

        if self._image_path != 'Assets/image_icon.png':
            if val_radio_button == 1:  # gray scale
                openCvManagment.gray_scale_image(self._picture_data[2])
            elif val_radio_button == 2:  # top
                openCvManagment.half_image(self._picture_data[2], openCvManagment.TOP)
            elif val_radio_button == 3:  # low
                openCvManagment.half_image(self._picture_data[2], openCvManagment.LOW)
            elif val_radio_button == 4:  # left
                openCvManagment.half_image(self._picture_data[2], openCvManagment.LEFT)
            elif val_radio_button == 5:  # right
                openCvManagment.half_image(self._picture_data[2], openCvManagment.RIGHT)
            elif val_radio_button == 6:
                self._client.recv_and_send(23, [self._picture_data[2]])
            elif val_radio_button == 8:
                openCvManagment.draw_blue_circles(self._picture_data[2])
            elif val_radio_button == 7:
                messagebox.showinfo('Pay Attention', 'Draw with your mouse and click \'m\' key\nto change the mode')
                openCvManagment.advanced_drawing(self._picture_data[2])
            elif val_radio_button == 9:
                openCvManagment.hsv_detection(self._picture_data[2])
        else:
            messagebox.showerror(title='ERROR', message='Image path does not exist..')
