from tkinter import *
from PIL import Image, ImageTk
from tkinter import ttk
from tkinter import messagebox
import Helper

class ListTags(object):
    def __init__(self, client, parent, pic_name):
        self._parent = parent
        self._client = client
        self._pic_name = pic_name
        self._win = Toplevel()
        self._win.title("List Tags")
        self._win.geometry("400x300")
        self._frame = Frame(self._win)
        self._album_icon = ImageTk.PhotoImage(Image.open('Assets/album_icon.png').resize((30, 30)))
        self._main_frame = Frame(self._win)
        self._main_frame.pack(fill=BOTH, expand=1)  # expand the frame to all root window size

        # Cretae A Canvas
        self._canvas = Canvas(self._main_frame)
        self._canvas.pack(side=LEFT, fill=BOTH, expand=1)

        # Add A Scrollbar to the Canvas
        self._my_scrollbar = ttk.Scrollbar(self._main_frame, orient=VERTICAL, command=self._canvas.yview)
        self._my_scrollbar.pack(side=RIGHT, fill=Y)

        # configure the canvas
        self._canvas.configure(yscrollcommand=self._my_scrollbar.set)
        self._canvas.bind('<Configure>', lambda e: self._canvas.configure(scrollregion=self._canvas.bbox("all")))

        # create Another frame inside the canvas
        self.second_frame = Frame(self._canvas)

        # add that new frame to a window in the canvas
        self._canvas.create_window((0, 0), window=self.second_frame, anchor="nw")
        self._canvas.pack()
        self._tags = []
        if self.show_tags() == -1:
            self._win.destroy()
        self._win.mainloop()

    def show_tags(self):
        data = [self._pic_name]
        code, message_from_server = self._client.recv_and_send(13, data)
        print('message from server ', message_from_server)
        if message_from_server[0] == 'ERROR':
            self._win.destroy()
            messagebox.showinfo(message='There is no user tegged in <' +self._pic_name + '>')
            return -1
        else:
            for ind, message in enumerate(message_from_server):
                user_data = Helper.parse_user(message)
                curr_tag = Label(self.second_frame,
                      text=user_data[1],
                      font=('Arial', 7, 'bold'),
                      borderwidth=4,
                      relief="solid", highlightcolor="white",
                      bd=3,
                      padx=5,
                      pady=5,
                      width=5,
                      height=5,
                      wraplength=50,
                      compound='bottom')
                self._tags.append(curr_tag)
                curr_tag.grid(row=ind // 3, column=ind % 3, padx=10, pady=10)
                # curr_album.bind("<Button-1>", self.show_pic_options)
                # curr_album.bind("<Button-3>", lambda e: print('delete'))
                # self._all_pictures.append(curr_album)
                return 1
