from tkinter import *
from PIL import Image, ImageTk
from tkinter import ttk
import Helper


class AlbumsUser(object):
    def __init__(self, client, user_data, parent):
        self._parent_win = parent
        self._user_name = user_data[1]
        self._win = Toplevel()
        self._win.title("Albums of user " + self._user_name)
        self._win.geometry('750x500')

        self._gallery_icon = ImageTk.PhotoImage(Image.open('Assets/gallery_logo.png').resize((30, 30)))
        self._image_icon_res = ImageTk.PhotoImage(Image.open('Assets/album_icon.png').resize((10, 10)))

        self._album_icon = ImageTk.PhotoImage(Image.open('Assets/album_icon.png').resize((30, 30)))
        self._win.iconphoto(False, self._gallery_icon)
        self._main_frame = Frame(self._win)
        self._main_frame.pack(fill=BOTH, expand=1)  # expand the frame to all root window size
        self._client = client

        # Cretae A Canvas
        self._canvas = Canvas(self._main_frame)
        self._canvas.pack(side=LEFT, fill=BOTH, expand=1)

        # Add A Scrollbar to the Canvas
        self._my_scrollbar = ttk.Scrollbar(self._main_frame, orient=VERTICAL, command=self._canvas.yview)
        self._my_scrollbar.pack(side=RIGHT, fill=Y)

        # configure the canvas
        self._canvas.configure(yscrollcommand=self._my_scrollbar.set)
        self._canvas.bind('<Configure>', lambda e: self._canvas.configure(scrollregion=self._canvas.bbox("all")))

        # create Another frame inside the canvas
        self.second_frame = Frame(self._canvas)

        # add that new frame to a window in the canvas
        self._canvas.create_window((0, 0), window=self.second_frame, anchor="nw", width=500)
        self._canvas.pack()
        self._all_albums = []
        self._albums_data = []
        code, list_albums = self._client.recv_and_send(6, [str(user_data[0])])
        print(code, list_albums)
        Helper.strlp_list(list_albums)
        self.get_albums(list_albums)
        self._win.mainloop()

    def get_albums(self, list_pictures):
        if len(list_pictures) >= 1 and list_pictures[0] != '':
            print('list pictures\n', list_pictures)
            for ind, album_text in enumerate(list_pictures):
                print(album_text)
                curr_album = Label(self.second_frame,
                      text=album_text[3:album_text.find(']')],
                      font=('Arial', 10, 'bold'),
                      borderwidth=4,
                      relief="solid", highlightcolor="white",
                      bd=3,
                      padx=10,
                      pady=10,
                      width=60,
                      height=60,
                      image=self._album_icon,
                      wraplength=50,
                      compound='bottom')
                #self._albums_data.append(album)
                curr_album.grid(row=ind // 3, column=ind % 3, padx=10, pady=10)
                #curr_album.bind("<Button-1>", self.show_pic_options)
                #curr_album.bind("<Button-3>", self.remove_picture)
                self._all_albums.append(curr_album)
                #print(ind, picture_text)
        else:
            pass
