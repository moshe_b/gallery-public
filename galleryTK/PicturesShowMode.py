from tkinter import *
from PIL import Image, ImageTk


class PicturesShowMode(object):
    def __init__(self, pictures_data):
        self._win = Toplevel()
        self._win.geometry('300x300')
        self._win.title('Show pictures')
        self._default_picture = ImageTk.PhotoImage(Image.open('Assets/image_icon.png').resize((300, 270)))

        self._pictures = self.get_pics(pictures_data)
        self._label_pic = Label(self._win, width=300, height=270, image=self._pictures[0])
        self._label_pic.image = self._pictures[0]
        self._label_pic.place(x=0, y=0)

        self._left_button = Button(self._win, text="<<", command=self.left_switch)
        self._left_button.place(x=0, y=280)
        self._right_button = Button(self._win, text=">>", command=self.right_switch)
        self._right_button.place(x=270, y=280)

        self._ind_pic_label = Label(self._win, text='0')
        self._ind_pic_label.place(x=130, y=280)
        self._pic_ind = 0

    def get_pics(self, pictures_data):
        pictures = []
        for data in pictures_data:
            try:
                pictures.append(ImageTk.PhotoImage(Image.open(data[2]).resize((300, 270))))
            except:
                pictures.append(self._default_picture)

        return pictures

    def left_switch(self):
        if self._pic_ind == 0:
            self._pic_ind = len(self._pictures) - 1
        else:
            self._pic_ind -= 1

        self._ind_pic_label.config(text=str(self._pic_ind))
        self._label_pic.config(image=self._pictures[self._pic_ind])

    def right_switch(self):
        if self._pic_ind == len(self._pictures) - 1:
            self._pic_ind = 0
        else:
            self._pic_ind += 1

        self._ind_pic_label.config(text=str(self._pic_ind))
        self._label_pic.config(image=self._pictures[self._pic_ind])

