from tkinter import *
from tkinter import messagebox
from tkinter import ttk
from PIL import Image, ImageTk
from PicturesWin import PicturesAlbum
from Helper import strlp_list


class AlbumsFrame(object):
    def __init__(self, parentFrame, client, win):
        self._frame = Frame(parentFrame)
        self._parent = win
        self._album_icon = ImageTk.PhotoImage(Image.open('Assets/album_icon.png').resize((30, 30)))
        self._main_frame = Frame(self._frame)
        self._main_frame.pack(fill=BOTH, expand=1)  # expand the frame to all root window size
        self._client = client

        # Cretae A Canvas
        self._canvas = Canvas(self._main_frame)
        self._canvas.pack(side=LEFT, fill=BOTH, expand=1)

        # Add A Scrollbar to the Canvas
        self._my_scrollbar = ttk.Scrollbar(self._main_frame, orient=VERTICAL, command=self._canvas.yview)
        self._my_scrollbar.pack(side=RIGHT, fill=Y)

        # configure the canvas
        self._canvas.configure(yscrollcommand=self._my_scrollbar.set)
        self._canvas.bind('<Configure>', lambda e: self._canvas.configure(scrollregion=self._canvas.bbox("all")))

        # create Another frame inside the canvas
        self.second_frame = Frame(self._canvas)

        # add that new frame to a window in the canvas
        self._canvas.create_window((0, 0), window=self.second_frame, anchor="nw", width=500)
        self._canvas.pack()
        self._all_albums = []
        self._del_album_id = -1
        self._albumsName_userId = []
        self._open_album = None

    def get_albums(self, list_albums):
        if len(list_albums) > 1:
            for ind, album_text in enumerate(list_albums):
                curr_album = Label(self.second_frame,
                  text=album_text[1:album_text.find(']')],
                  font=('Arial', 10, 'bold'),
                  borderwidth=4,
                  relief="solid", highlightcolor="white",
                  bd=3,
                  padx=10,
                  pady=10,
                  width=90,
                  height=90,
                  wraplength=50,
                  image=self._album_icon,
                  compound='bottom')
                curr_album.image = self._album_icon
                text = "Name: " + album_text[1:album_text.find(']')] + "\nCreator: " + album_text[album_text.find("by")+3: album_text.find('\n')]
                self._albumsName_userId.append((text[text.find("Name: ")+6:text.find('\n')],
                                                text[text.find("@")+1:len(text)]))
                curr_album.grid(row=ind // 3, column=ind % 3, padx=10, pady=10)
                curr_album.bind("<Button-1>", self.get_albums_pictures)
                curr_album.bind("<Button-3>", self.delete_album)
                self._all_albums.append(curr_album)
        else:
            pass

    def delete_album(self, event):
        album = event.widget
        if messagebox.askyesno(title='Important', message='Are you sure you want to delete this album?'):
            ind_list = 3*album.grid_info()['row'] + album.grid_info()['column']
            data_pic = self._albumsName_userId[ind_list]
            code, message_from_server = self._client.recv_and_send(4, [data_pic[1], data_pic[0]])
            self._albumsName_userId = []
            for label in self._all_albums:
                label.destroy()
            self._all_albums = []
            self.get_albums(message_from_server)

    def delete_all_albums(self):
        for label in self._all_albums:
            label.destroy()

    def get_albums_pictures(self, event):
        self._parent.withdraw()
        album = event.widget
        album_name = album['text']
        ind_list = 3*album.grid_info()['row'] + album.grid_info()['column']
        print("ind = ", ind_list)
        print("self._albumsName_userId[ind_list] = ", self._albumsName_userId[ind_list] )
        code, l = self._client.recv_and_send(2,
                [self._albumsName_userId[ind_list][1], album_name])
        print("album name = ", album_name)
        self._open_album = PicturesAlbum(self._client, album_name, self._parent)
