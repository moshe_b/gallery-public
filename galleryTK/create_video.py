from tkinter import *
from tkinter import filedialog
from openCvManagment import create_video
from tkinter import messagebox


class CreateVideo(object):
    def __init__(self, client, parent):
        self._win = Toplevel()
        self._win.geometry("330x300")
        self._win.title('Create video')

        self._paernt = parent
        self._client = client
        self._video_name_label = Label(self._win, text="Enter a video name: ")
        self._video_name_label.place(x=20, y=25)

        self._video_name_entry = Entry(self._win, width=30)
        self._video_name_entry.place(x=130, y=25)

        self._choose_dir = Button(self._win, text="Choose location", command=self.get_location)
        self._choose_dir.place(x=40, y=60)

        self._dir_chosen = Label(self._win, text="YOUR VIDEO LOCATION")
        self._dir_chosen.place(x=40, y=100)

        self._start_video = Button(self._win, text="Start video", command=self.create_video)
        self._start_video.place(x=40, y=140)

    def get_location(self):
        dir = filedialog.askdirectory()
        if dir != "":
            self._dir_chosen.config(text=str(dir))

    def create_video(self):
        if self._dir_chosen["text"] == "YOUR VIDEO LOCATION":
            messagebox.showerror('ERROR', 'You must choose directory')
        elif self._video_name_entry.get() == "":
            messagebox.showerror('ERROR', 'You must enter video name')
        else:
            video_path = self._dir_chosen["text"]+"\\"+self._video_name_entry.get()
            create_video(video_path)
            code, pics = self._client.recv_and_send(7, [self._video_name_entry.get(), video_path])
            self._paernt.get_pictures(pics)
