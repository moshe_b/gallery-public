import cv2
import numpy as np

TOP = 0
LOW = 1
LEFT = 2
RIGHT = 3


def gray_scale_image(path_image):
    print(path_image)
    img = cv2.imread(path_image, 0)
    cv2.imshow('gray_scale', img)

    k = cv2.waitKey(0)

    if k == 27:  # esc key
        cv2.destroyAllWindows()
    elif k == ord('s'):
        cv2.imwrite(path_image, img)
        cv2.destroyAllWindows()


def half_image(path_image, code):
    img = cv2.imread(path_image, 1)
    width = img.shape[1]  # columns
    height = img.shape[0]  # rows
    win_name = ''
    half = None
    if code == TOP:
        win_name = 'half top'
        half = img[:height//2, :]
    elif code == LOW:
        win_name = 'half low'
        half = img[height//2:, :]
    elif code == LEFT:
        win_name = 'half left'
        half = img[:, :width//2]
    elif code == RIGHT:
        win_name = 'half right'
        half = img[:, width//2:]

    cv2.imshow(win_name, half)

    k = cv2.waitKey(0)

    if k == 27:  # esc key
        cv2.destroyAllWindows()
    elif k == ord('s'):
        cv2.imwrite(path_image, half)
        cv2.destroyAllWindows()


img = None


def draw_circle(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDBLCLK:
        # print("img = ", img)
        cv2.circle(img, (x, y), 20, (255, 0, 0), -1)


def draw_blue_circles(path_img):
    global img
    img = cv2.imread(path_img)

    cv2.namedWindow('image')
    cv2.setMouseCallback('image', draw_circle)

    while(True):
        cv2.imshow('image', img)
        if cv2.waitKey(20) & 0xFF == 27:
            break
    cv2.destroyAllWindows()


drawing = False # true if mouse is pressed
mode = True # if True, draw rectangle. Press 'm' to toggle to curve
ix, iy = -1, -1


# mouse callback function
def draw_advanced_call_back(event,x,y,flags,param):
    global ix, iy, drawing, mode
    global img
    print('drawing advanced')
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix, iy = x, y

    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing == True:
            if mode == True:
                print('ss')
                cv2.rectangle(img, (ix, iy), (x, y), (0, 255, 0), -1)
            else:
                print('s')
                cv2.circle(img, (x, y), 5, (0, 0, 255), -1)

    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        if mode == True:
            cv2.rectangle(img, (ix, iy), (x, y), (0, 255, 0), -1)
        else:
            cv2.circle(img, (x, y), 5, (0, 0, 255), -1)


def advanced_drawing(img_path):
    print('advanced')
    global img
    global mode
    img = cv2.imread(img_path)
    cv2.namedWindow('image')
    cv2.setMouseCallback('image', draw_advanced_call_back)
    while True:
        cv2.imshow('image', img)
        k = cv2.waitKey(1) & 0xFF
        if k == ord('m'):
            mode = not mode
        elif k == 27:
            break

    cv2.destroyAllWindows()


def create_video(video_name):
    try:
        cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)  # catch the opetion to camera
        # the two lines make the opetion to save the vides
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        out = cv2.VideoWriter(f'{video_name}', fourcc, 20.0, (640, 480))

        # set the camera parameters

        print(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        print(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

        # set the camera size with and height
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, 720)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1280)

        while cap.isOpened(): #check if opened
            ret, frame = cap.read() # read frame by frame from camera
            if ret:  # if succed
                out.write(frame)  # write the frame to new video
                #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) # for gray color
                cv2.imshow('video', frame)  # show the frame in gray mode

                if cv2.waitKey(1) == ord('q'):  # wait for q key to finish
                    break
            else:
                break

        cap.release()  # must realese those object at the end
        out.release()
        cv2.destroyAllWindows()  # close all the windows that opened
    except:
        pass


def nothing(x):
    pass


def hsv_detection_video(video_name):
    cap = cv2.VideoCapture(0)
    _, frame = cap.read()
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter(f'{video_name}', fourcc, 20.0, (640, 480))

    cv2.namedWindow("Tracking")
    cv2.createTrackbar("LH", "Tracking", 0, 255, nothing)
    cv2.createTrackbar("LS", "Tracking", 0, 255, nothing)
    cv2.createTrackbar("LV", "Tracking", 0, 255, nothing)
    cv2.createTrackbar("UH", "Tracking", 255, 255, nothing)
    cv2.createTrackbar("US", "Tracking", 255, 255, nothing)
    cv2.createTrackbar("UV", "Tracking", 255, 255, nothing)

    while True:
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        l_h = cv2.getTrackbarPos("LH", "Tracking")
        l_s = cv2.getTrackbarPos("LS", "Tracking")
        l_v = cv2.getTrackbarPos("LV", "Tracking")

        u_h = cv2.getTrackbarPos("UH", "Tracking")
        u_s = cv2.getTrackbarPos("US", "Tracking")
        u_v = cv2.getTrackbarPos("UV", "Tracking")

        l_b = np.array([l_h, l_s, l_v])  # [l_h, l_s, l_v]
        u_b = np.array([u_h, u_s, u_v])  # [u_h, u_s, u_v]
        mask = cv2.inRange(hsv, l_b, u_b)  # create mask

        # it's do frame & mask
        # every thing that get 1 with the color
        res = cv2.bitwise_and(frame, frame, mask=mask)
        out.write(res)
        cv2.imshow("frame", frame)
        cv2.imshow("mask", mask)
        cv2.imshow("res", res)


        key = cv2.waitKey(0)
        if key == 27:
            break
    cv2.destroyAllWindows()

