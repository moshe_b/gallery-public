from tkinter import *
from TagUser import TagUser
from ShowPictureWin import ShowPicture
from UnTag import UnTagUser
from listTags import *
from AlbumsUser import AlbumsUser
from PicturesWin import PicturesAlbum

class UserOptions(object):
    def __init__(self, user_data, client):
        self._win = Toplevel()
        self._user_name = user_data[1]
        self._user_data = user_data
        self._client = client
        self._win.title(f'User {user_data[1]} Options')
        self._win.geometry("300x300")
        self._var = IntVar()
        self._message = Label(self._win, text='Choose you option down bellow')
        self._message.place(x=70, y=10)
        self._delete_user = Radiobutton(self._win, text='see user\'s album', variable=self._var, value=1)
        self._delete_user.place(x=5, y=50)
        self._see_tagged_pics = Radiobutton(self._win, text=f'See user details', variable=self._var, value=2)
        self._see_tagged_pics.place(x=5, y=90)
        self._choice = Button(self._win, text='Go->', command=self.go)
        self._choice.place(x=10, y=130)
        self._win.mainloop()

    def go(self):
        choice = self._var.get()
        print(choice)
        if choice == 1:
            win = AlbumsUser(self._client, self._user_data, self)
        elif choice == 2:
            message = f'Name: {self._user_name}\nId: {self._user_data[0]}'
            messagebox.showinfo(title='User info', message=message)

