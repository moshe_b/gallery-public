from tkinter import *
from PIL import ImageTk, Image
from tkinter import ttk
from AlbumsUser import AlbumsUser
from tkinter import messagebox
from UserOption import UserOptions
import Helper


class UsersWin(Frame):
    def __init__(self, parent, client):
        Frame.__init__(self, parent)
        self._user_icon = ImageTk.PhotoImage(Image.open('Assets/user_icon.png').resize((30, 30)))
        self._main_frame = Frame(parent)
        self._main_frame.pack(fill=BOTH, expand=1)  # expand the frame to all root window size
        self._client = client
        self._parent = parent
        # Cretae A Canvas
        self._canvas = Canvas(self._main_frame)
        self._canvas.pack(side=LEFT, fill=BOTH, expand=1)

        # Add A Scrollbar to the Canvas
        self._my_scrollbar = ttk.Scrollbar(self._main_frame, orient=VERTICAL, command=self._canvas.yview)
        self._my_scrollbar.pack(side=RIGHT, fill=Y)

        # configure the canvas
        self._canvas.configure(yscrollcommand=self._my_scrollbar.set)
        self._canvas.bind('<Configure>', lambda e: self._canvas.configure(scrollregion=self._canvas.bbox("all")))

        # create Another frame inside the canvas
        self.second_frame = Frame(self._canvas)

        # add that new frame to a window in the canvas
        self._canvas.create_window((0, 0), window=self.second_frame, anchor="nw", width=500)
        self._canvas.pack()
        self._all_users = []
        self._usersName_userId = []
        # self._open_album = None

    def get_users(self, list_albums):
        if len(list_albums) >= 1:
            for ind, user_text in enumerate(list_albums):
                print(user_text)
                user_data = Helper.parse_user(user_text)
                self._usersName_userId.append(user_data)
                curr_album = Label(self.second_frame,
                  text=user_data[1],
                  font=('Arial', 10, 'bold'),
                  borderwidth=4,
                  relief="solid", highlightcolor="white",
                  bd=3,
                  padx=15,
                  pady=50,
                  width=50,
                  height=10,
                  wraplength=50,
                  image=self._user_icon,
                  compound='bottom')
                curr_album.grid(row=ind // 3, column=ind % 3, padx=10, pady=10)
                curr_album.bind("<Button-1>", self.see_options)
                curr_album.bind("<Button-3>", self.remove_user)
                self._all_users.append(curr_album)
        else:
            pass

    def see_options(self, event):
        user = event.widget
        ind_list = 3*user.grid_info()['row'] + user.grid_info()['column']
        print('inde_list', ind_list)
        user_data = list(self._usersName_userId[ind_list])
        print(user_data)
        win = UserOptions(user_data, self._client)

    def remove_user(self, event):
        pic = event.widget
        if messagebox.askyesno(title='Important', message='Are you sure you want to delete this user?'):
            ind_list = 3*pic.grid_info()['row'] + pic.grid_info()['column']
            data_user = self._usersName_userId[ind_list]
            code, message_from_server = self._client.recv_and_send(15, [str(data_user[0]), str(data_user[1])])
            self._usersName_userId = []
            for label in self._all_users:
                label.destroy()
            self._all_users = []
            self.get_users(message_from_server)

    def remove_all_users(self):
        for user in self._all_users:
            user.destroy()
