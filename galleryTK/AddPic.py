from tkinter import *
from PIL import Image, ImageTk
from tkinter import messagebox


class AddPic(object):
    def __init__(self, parent, client, pictures_obj):
        self._win = Toplevel()
        self._pictures_obj = pictures_obj
        self._parent_win = parent
        self._sock = client
        self._win.title('Create Picture')
        self._win.geometry("250x200")
        self._win.iconphoto(False, PhotoImage(file='Assets/gallery_logo.png'))
        self._user_icon = ImageTk.PhotoImage(Image.open('Assets/user_icon.png').resize((50, 50)))
        self._pic_name_label = Label(self._win, text='Picture Name: ')
        self._pic_name_label.grid(row=0, column=0)
        self._pic_name_entry = Entry(self._win, width=25)
        self._pic_name_entry.grid(row=0, column=1)

        self._pic_path_label = Label(self._win, text='Picture Path: ')
        self._pic_path_label.grid(row=1, column=0)
        self._pic_path_entry = Entry(self._win, width=25)
        self._pic_path_entry.grid(row=1, column=1)
        self._button_create = Button(self._win, text='Create+', width=20, command=self.add_picture).grid(row=2, column=0, columnspan=2, padx=10, pady=10)
        self._label_user = Label(self._win)
        self._label_user.image = self._user_icon
        self._label_user.config(image=self._user_icon)
        self._label_user.grid(row=3, column=0)
        self._win.mainloop()

    def add_picture(self):
        pic_path = self._pic_path_entry.get()
        pic_name = self._pic_name_entry.get()
        data = [pic_name, pic_path]
        if pic_name == "":
            messagebox.showerror(title='ERROR', message='You must enter the picture\'s name')
        elif pic_path == "":
            messagebox.showerror(title='ERROR', message='You must enter the picture\'s path')
        else:
            print(data)
            code, list_pictures = self._sock.recv_and_send(7, data)
            all_pictures = self._pictures_obj._all_pictures
            for label in all_pictures:
                label.destroy()
            self._pictures_obj._pictures_data = []
            self._pictures_obj._all_pictures = []
            self._pictures_obj.get_pictures(list_pictures)
            #self._parent_win.deiconify()
            #self._win.destroy()
