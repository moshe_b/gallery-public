from tkinter import *
from tkinter import ttk
from PIL import Image, ImageTk
from GalleryFrame import GalleryFrame
from AlbumsFrame import AlbumsFrame
from client import Client
from createAlbumWin import CreateAlbumWin
from UsersWin import UsersWin
import Helper
from createUserWin import CreateUser
from getStatistics import GetStatistics
from send_mail import SendEmail


class Gallery(Tk):
    def __init__(self, width=750, height=500):
        Tk.__init__(self)
        # images and stuff
        self._userImg = ImageTk.PhotoImage(Image.open('Assets/user_icon.png').resize((20, 20)))
        self._albumImg = ImageTk.PhotoImage(Image.open('Assets/album_icon.png').resize((20, 20)))
        self.iconphoto(False, PhotoImage(file='Assets/gallery_logo.png'))

        self._menue_bar = Menu(self)
        self.config(menu=self._menue_bar)
        # remove the line separate
        self._create_menu = Menu(self._menue_bar, tearoff=0,
                              font=("My Boli", 10))
        self._statistics = Menu(self._menue_bar, tearoff=0,
                              font=("My Boli", 10))
        self._queries = Menu(self._menue_bar, tearoff=0,
                              font=("My Boli", 10))
        self.create_menu()

        self.title("Gallery")
        self.geometry(f'{width}x{height}')
        self._client = Client()

        self._tabs = ttk.Notebook(self, width=width, height=height)
        self._frames = self.create_frames()  # dict rtype
        self.bind("<<NotebookTabChanged>>", self.handle_switch_tabs)
        self._gallery = GalleryFrame(self._frames['Gallery'])
        self._albums = AlbumsFrame(self._frames['Albums'], self._client, self)
        self._users = UsersWin(self._frames['Users'], self._client)
        self._connect = SendEmail(self._frames['Connect'])
        self.pack_everything()
        self.mainloop()

    def create_frames(self):
        frames = {"Gallery": Frame(self._tabs),
                  "Albums": Frame(self._tabs),
                  "Users": Frame(self._tabs),
                  "Connect": Frame(self._tabs)}
        for frame in frames:
            self._tabs.add(frames[frame], text=frame)

        return frames

    def pack_everything(self):
        self._tabs.pack()
        self._gallery.pack()
        self._albums._frame.pack(fill=BOTH, expand=1)

    def handle_switch_tabs(self, event):
        selected_tab = event.widget.select()
        tab_text = event.widget.tab(selected_tab, "text")
        if tab_text == "Albums":
            code, list_albums = self._client.recv_and_send(5, [])
            self._albums.delete_all_albums()
            self._albums.get_albums(list_albums)
        elif tab_text == "Users":
            self._users.remove_all_users()
            code, list_users = self._client.recv_and_send(16, [])
            Helper.strlp_list(list_users)
            self._users.get_users(list_users)

    def create_menu(self):
        self._menue_bar.add_cascade(label="Create", menu=self._create_menu)
        self._menue_bar.add_cascade(label='Statistics', menu=self._statistics)
        self._menue_bar.add_cascade(label='Queries', menu=self._queries)
        # add sub items to the file menu
        self._create_menu.add_command(label="Create Album", command=self.create_album, image=self._albumImg, compound='right')
        self._create_menu.add_separator()  # add separate line
        self._create_menu.add_command(label="Create User", command=self.create_user,image=self._userImg, compound='right')

        self._statistics.add_command(label="Show Statistics", command=self.show_statistics, compound='right')
        self._queries.add_command(label="Top Tagged user", command=self.top_tagged_user, compound='right')
        self._queries.add_command(label="Top Tagged Picture", command=self.top_tagged_picture, compound='right')

    def create_album(self):
        new_win = CreateAlbumWin(self, self._client)

    def create_user(self):
        self.withdraw()
        win = self
        #self.destroy()
        new_win = CreateUser(self, self._client)

    def show_statistics(self):
        win = GetStatistics(self, self._client)
        win.get_statistics()

    def top_tagged_user(self):
        code, message_from_server = self._client.recv_and_send(18, [])
        win = Toplevel(background='#00ff00')
        win.geometry("200x150")
        win.title('Top Tagged user')
        user_img = ImageTk.PhotoImage(Image.open('Assets/user_icon.png').resize((30, 30)))
        image = Label(win, image=user_img,
                      compound='bottom', text=message_from_server[0])
        image.image = user_img
        image.place(x=20, y=50)
        win.mainloop()

    def top_tagged_picture(self):
        code, message_from_server = self._client.recv_and_send(19, [])
        win = Toplevel(background='#ff0000')
        win.geometry("250x150")
        win.title('Top Tagged Picture')
        pic_img = ImageTk.PhotoImage(Image.open('Assets/image_icon.png').resize((30, 30)))
        image = Label(win, image=pic_img,
                      compound='bottom', text=message_from_server[0])
        image.image = pic_img
        image.place(x=20, y=50)
        win.mainloop()

