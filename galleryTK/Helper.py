def strlp_list(my_list):
    for ind, val in enumerate(my_list):
        my_list[ind] = val.strip()


def parse_picture_data(str_picture, kind_parse):
    """
    parse picture data into tuple(ID, NAME, LOCATION, CREATION_DATE, ALBUM_ID)
    :param str_picture: str
    :return:
    """
    if kind_parse == 0:
        str_picture = str_picture.strip()
        list_picture = str_picture.split(' ')
        if list_picture[0] == '':
            return ()
        print('list: ', list_picture)
        pic_id = int(list_picture[2][1:len(list_picture[2])-1])
        pic_name = list_picture[4][0:list_picture[4].find('\t')]
        pic_location = list_picture[5][1:list_picture[5].find(']')]
        pic_creation_date = list_picture[7][1:list_picture[7].find(']')]
        pic_tags = list_picture[8][1:list_picture[8].find(']')]

        picture = (pic_id, pic_name, pic_location, pic_creation_date, pic_tags)
        print(picture)
        return picture
    else:  # for tags of picture
        str_picture = str_picture.strip()
        pic_name_loc = str_picture[str_picture.find('['):str_picture.find(']')+1]
        print(pic_name_loc)
        return pic_name_loc.strip('][').split(', ') # return the name and the location of the picture


def parse_statistics_data(list_data):
    str_l = list_data[0]
    str_l = str_l[str_l.find('Count of Albums Tagged: '):]
    count_albums_tagged = int(str_l[len('Count of Albums Tagged: '):str_l.find('\n')])
    str_l = str_l[str_l.find('Count of Tags: '):]
    count_of_tags = int(str_l[len('Count of Tags: '):str_l.find('\n')])
    str_l = str_l[str_l.find('Avarage Tags per Alboum: '):]
    print(str_l[len('Avarage Tags per Alboum: '):str_l.find('\n')])
    tags_per_album = float(str_l[len('Avarage Tags per Alboum: '):str_l.find('\n')])
    parse = (count_albums_tagged, count_of_tags, tags_per_album)
    print(parse)
    return parse


def parse_user(user_str):
    user_str = user_str.strip()
    user_str = user_str[user_str.find("+ &") + 3:len(user_str)]
    id = int(user_str[1:user_str.find(' ')])
    user_name = user_str[user_str.find(" - ")+3:len(user_str)]
    return id, user_name


def parse_user_tags(user_tag):
    user_tag = user_tag.strip()
    pic_name = user_tag[user_tag.find("["): user_tag.find("]")]
