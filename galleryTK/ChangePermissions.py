from tkinter import *
from tkinter import messagebox


class ChangePermissions(object):
        def __init__(self, pic_data, client):
            self._win = Toplevel()
            self._pic_name = pic_data[1]
            self._pic_data = pic_data
            self._client = client
            self._win.title(f'Change permissions of {pic_data[1]}')
            self._win.geometry("300x300")
            self._var = IntVar()
            self._message = Label(self._win, text='Choose you option down bellow')
            self._message.place(x=70, y=10)
            self._read_only = Radiobutton(self._win, text='Read_Only', variable=self._var, value=1)
            self._read_only.place(x=5, y=50)
            self._read_write = Radiobutton(self._win, text='Read+Write', variable=self._var, value=2)
            self._read_write.place(x=5, y=90)
            self._choice = Button(self._win, text='Change->', command=self.change)
            self._choice.place(x=10, y=130)
            self._win.mainloop()

        def change(self):
            choice = self._var.get()
            message_from_server = []
            if choice == 1:
                code, message_from_server = self._client.recv_and_send(21, [self._pic_data[2]]) # send the path
                messagebox.showinfo(title='Permission', message='The image have only read only permission')
            elif choice == 2:
                code, message_from_server = self._client.recv_and_send(22, [self._pic_data[2]]) # send the path
                messagebox.showinfo(title='Permission', message='The image have read and write permission')

