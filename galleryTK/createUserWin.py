from tkinter import *
from PIL import Image, ImageTk
from tkinter import messagebox


class CreateUser(object):
    def __init__(self, parent, client):
        self._win = Toplevel()
        self._parent_win = parent
        self._sock = client
        self._win.title('Create User')
        self._win.geometry("250x200")
        self._win.iconphoto(False, PhotoImage(file='Assets/gallery_logo.png'))
        self._user_icon = ImageTk.PhotoImage(Image.open('Assets/user_icon.png').resize((50, 50)))
        self._user_name_label = Label(self._win, text='User Name: ')
        self._user_name_label.grid(row=0, column=0)
        self._user_name_entry = Entry(self._win, width=25)
        self._user_name_entry.grid(row=0, column=1)
        self._button_create = Button(self._win, text='Create+', width=20, command=self.add_user).grid(row=1, column=0, columnspan=2, padx=10, pady=10)
        self._label_user = Label(self._win, text='hey')
        self._label_user.image = self._user_icon
        self._label_user.config(image=self._user_icon)
        self._label_user.grid(row=2, column=0)
        self._win.mainloop()

    def add_user(self):
        user_name = self._user_name_entry.get()
        if user_name == "":
            messagebox.showerror('ERROR', 'You must enter a user name')
        else:
            data = [user_name]
            print(data)
            code, list_albums = self._sock.recv_and_send(14, data)
            self._parent_win._users.get_users(list_albums)
            self._parent_win.deiconify()
            self._win.destroy()

