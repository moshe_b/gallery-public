import socket

SERVER_IP = '127.1.0.0'
SERVER_PORT = 5555


class Client(object):
    def __init__(self):
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._server_address = (SERVER_IP, SERVER_PORT)
        self._id = self.connect()
        print(self._id)

    def connect(self):
        try:
            self._sock.connect(self._server_address)
            return self._sock.recv(1024).decode()
        except Exception as e:
            print('Error', e)

    def recv_and_send(self, code, list_data_to_server):
        print("code = ", code)
        print(list_data_to_server)
        message_to_server = str(code).zfill(3) + '&' + '&'.join(list_data_to_server)
        print(message_to_server)
        print(self._sock.send(message_to_server.encode()))

        message_from_server = self._sock.recv(1024).decode()
        if message_from_server == "":
            return

        print(message_from_server)
        code = int(message_from_server[0:3])
        message_list = message_from_server[4:].split('&')

        return code, message_list
