import matplotlib.pyplot as plt


def show_statistics_win(list_sta):
    x = ['Count of Albums Tagged', 'Count of Tags', 'Avarage Tags per Alboum']
    plt.bar(x, list_sta, 0.4)

    plt.xlabel("Category")
    plt.ylabel("Amount")
    plt.title('Statistics')

    plt.show()


def top_tagged_user(data):
    x = ['Top Tagged User']
    plt.bar(x, data, 0.4)

    plt.xlabel("Category")
    plt.ylabel("Tags")
    plt.title('Top Tagged User')

    plt.show()


