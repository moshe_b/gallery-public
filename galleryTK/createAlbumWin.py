from tkinter import *
from tkinter import messagebox

class CreateAlbumWin(Tk):
    def __init__(self, parent, socket):
        Tk.__init__(self)
        self._parent_win = parent
        self._sock = socket
        self.title('Create Album')
        self.geometry("300x300")
        #self.iconphoto(False, PhotoImage(file='Assets/gallery_logo.png'))
        self._user_id_label = Label(self, text='User Id: ')
        self._user_id_label.grid(row=0, column=0, padx=10, pady=10)
        self._user_id_entry = Entry(self, width=50)
        self._user_id_entry.grid(row=0, column=1)
        self._album_name_label = Label(self, text='Album Name: ')
        self._album_name_label.grid(row=1, column=0)
        self._album_name_entry = Entry(self, width=50)
        self._album_name_entry.grid(row=1, column=1)
        self._button_delete = Button(self, text='Create+', width=20, command=self.create_album).grid(row=2, column=0, columnspan=2, padx=10, pady=10)
        self.mainloop()

    def create_album(self):
        album_name = self._album_name_entry.get()
        user_id = self._user_id_entry.get()
        if album_name == "" or user_id == "":
            messagebox.showerror(title='ERROR', message='You must fill the both fields')
        elif not user_id.isdigit():
            messagebox.showerror(title='ERROR', message='User id must be a number')
        else:
            data = [user_id, album_name]
            print(data)
            code, list_albums = self._sock.recv_and_send(1, data)
            if len(list_albums) > 0 and list_albums[0] == "ERROR":
                messagebox.showerror('ERROR', list_albums[1])
            else:
                self._parent_win._albums.get_albums(list_albums)
                self._parent_win.deiconify()
                self.destroy()

