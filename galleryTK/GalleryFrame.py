from tkinter import *

ABOUT_ME = "Developer: Moshe Buznah\nBurned: 10/1/2003\nCompany: Magshimim"


class GalleryFrame(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        self._about_me_frame = Frame(self, width=300, height=300, background="bisque")
        self._label_about_me = Label(self._about_me_frame, bg="#000000", fg="#00FF00")
        self._label_about_me['text'] = ABOUT_ME
        self._me_icon_img = PhotoImage(file='Assets/me.png')

        self._change_icon = Frame(self, width=300, height=300)
        self._image_label = Label(self._change_icon, width=300, height=300,
                                  image=self._me_icon_img)
        self.pack_everything()

    def pack_everything(self):
        self._about_me_frame.pack(padx=30, side=LEFT)
        self._label_about_me.place(x=30, y=30)
        self._image_label.pack()
        self._change_icon.pack(padx=30, side=RIGHT)
