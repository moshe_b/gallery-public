from tkinter import *
from tkinter import messagebox
from PIL import ImageTk, Image
import PicturesWin


class TagsPicturesUser(object):
    def __init__(self, parent, client):
        self._win = Toplevel()
        self._parent_win = parent
        self._sock = client
        self._win.title('Create User')
        self._win.geometry("250x200")
        self._win.iconphoto(False, PhotoImage(file='Assets/gallery_logo.png'))
        self._user_icon = ImageTk.PhotoImage(Image.open('Assets/user_icon.png').resize((50, 50)))
        self._user_name_label = Label(self._win, text='User Id: ')
        self._user_name_label.grid(row=0, column=0)
        self._user_id_entry = Entry(self._win, width=25)
        self._user_id_entry.grid(row=0, column=1)
        self._button_create = Button(self._win, text='See Pictures+', width=20, command=self.tags_picture_user).grid(row=1, column=0, columnspan=2, padx=10, pady=10)
        self._label_user = Label(self._win)
        self._label_user.image = self._user_icon
        self._label_user.config(image=self._user_icon)
        self._label_user.grid(row=2, column=0)
        self._win.mainloop()

    def tags_picture_user(self):
        user_id = self._user_id_entry.get()
        if user_id == "":
            messagebox.showerror('ERROR', 'You must enter a user id')
        elif not user_id.isdigit():
            messagebox.showerror('ERROR', 'User id must be number')
        else:
            data = [user_id]
            print(data)
            code, list_pictures = self._sock.recv_and_send(20, data)
            if list_pictures[0] == "ERROR":
                messagebox.showerror('ERROR', list_pictures[1])
            else:
                win = PicturesWin.PicturesAlbum(self._sock, "", self._win, 0)
                win.get_pictures(list_pictures, kind_parse=1)
                win._win.mainloop()
                self._parent_win.deiconify()
                self._win.destroy()
