from tkinter import *
from PIL import ImageTk, Image
from tkinter import ttk
from tkinter import messagebox
import Helper
from ShowPictureWin import ShowPicture
from PictureOptions import PictureOptions
from AddPic import AddPic
from TagsPictureUser import TagsPicturesUser
from PicturesShowMode import PicturesShowMode
from create_video import CreateVideo


class PicturesAlbum(object):
    def __init__(self, client, albumName, parent, sign=1):
        self._parent_win = parent
        self._album_name = albumName
        self._win = Toplevel()
        self._win.title("Pictures of album " + albumName)
        self._win.geometry('750x500')

        self._menue_bar = Menu(self._win)
        self._create_menu = Menu(self._menue_bar, tearoff=0,
                              font=("My Boli", 10))
        self._win.config(menu=self._menue_bar)
        self._tags = Menu(self._menue_bar, tearoff=0,
                              font=("My Boli", 10))
        self._show = Menu(self._menue_bar, tearoff=0,
                              font=("My Boli", 10))

        self._win.config(menu=self._menue_bar)
        self.create_menu()

        self._gallery_icon = ImageTk.PhotoImage(Image.open('Assets/gallery_logo.png').resize((30, 30)))
        self._image_icon_res = ImageTk.PhotoImage(Image.open('Assets/image_icon.png').resize((10, 10)))

        self._image_icon = ImageTk.PhotoImage(Image.open('Assets/image_icon.png').resize((30, 30)))
        self._win.iconphoto(False, self._gallery_icon)
        self._main_frame = Frame(self._win)
        self._main_frame.pack(fill=BOTH, expand=1)  # expand the frame to all root window size
        self._client = client

        # Cretae A Canvas
        self._canvas = Canvas(self._main_frame)
        self._canvas.pack(side=LEFT, fill=BOTH, expand=1)

        # Add A Scrollbar to the Canvas
        self._my_scrollbar = ttk.Scrollbar(self._main_frame, orient=VERTICAL, command=self._canvas.yview)
        self._my_scrollbar.pack(side=RIGHT, fill=Y)

        # configure the canvas
        self._canvas.configure(yscrollcommand=self._my_scrollbar.set)
        self._canvas.bind('<Configure>', lambda e: self._canvas.configure(scrollregion=self._canvas.bbox("all")))

        # create Another frame inside the canvas
        self.second_frame = Frame(self._canvas)

        # add that new frame to a window in the canvas
        self._canvas.create_window((0, 0), window=self.second_frame, anchor="nw", width=500)
        self._canvas.pack()
        self._all_pictures = []
        self._del_album_id = -1
        self._pictures_data = []
        if sign == 1:
            code, list_pictures = self._client.recv_and_send(10, [self._album_name])
            print(code, list_pictures)
            self._win.protocol("WM_DELETE_WINDOW", self.close_win)
            Helper.strlp_list(list_pictures)
            self.get_pictures(list_pictures)

    def get_pictures(self, list_pictures, kind_parse=0):
        if len(list_pictures) >= 1 and list_pictures[0] != '':
            print('list pictures\n', list_pictures)
            for ind, picture_text in enumerate(list_pictures):
                picture_data = Helper.parse_picture_data(picture_text, kind_parse)
                print(picture_data)
                curr_album = Label(self.second_frame,
                      text=picture_data[1 if kind_parse == 0 else 0],
                      font=('Arial', 10, 'bold'),
                      borderwidth=4,
                      relief="solid", highlightcolor="white",
                      bd=3,
                      padx=10,
                      pady=10,
                      width=40,
                      height=40,
                      image=self._image_icon,
                      wraplength=50,
                      compound='bottom')
                self._pictures_data.append(picture_data)
                curr_album.grid(row=ind // 3, column=ind % 3, padx=10, pady=10)
                curr_album.bind("<Button-1>", self.show_pic_options)
                curr_album.bind("<Button-3>", self.remove_picture)
                self._all_pictures.append(curr_album)
                #print(ind, picture_text)
        else:
            pass

    def close_win(self):
        if messagebox.askyesno(title='close?', message='Do you want to close this window?\nits will also close the album'):
            self._client.recv_and_send(3, [])
            self._win.destroy()
            self._parent_win.deiconify()

    def show_pic_options(self, event):
        pic = event.widget
        ind_list = 3*pic.grid_info()['row'] + pic.grid_info()['column']
        dataSave = list(self._pictures_data[ind_list])
        pic_option_win = PictureOptions(dataSave, self._client)

    def remove_picture(self, event):
        pic = event.widget
        if messagebox.askyesno(title='Important', message='Are you sure you want to delete this picture?'):
            ind_list = 3*pic.grid_info()['row'] + pic.grid_info()['column']
            data_pic = self._pictures_data[ind_list]
            code, message_from_server = self._client.recv_and_send(8, [data_pic[1]])
            self._pictures_data = []
            for label in self._all_pictures:
                label.destroy()
            self._all_pictures = []
            self.get_pictures(message_from_server)

    def create_menu(self):
        self._menue_bar.add_cascade(label="Add+", menu=self._create_menu)
        self._menue_bar.add_cascade(label="Tags+", menu=self._tags)
        self._menue_bar.add_cascade(label="Show+", menu=self._show)

        self._create_menu.add_command(label="Create Picture", command=self.create_pic, compound='right')
        self._create_menu.add_command(label="Create Video", command=self.create_video, compound='right')
        self._create_menu.add_command(label="Create Video hsv", command=self.create_video, compound='right')
        self._tags.add_command(label="Show Tags", command=self.show_tags, compound='right')
        self._show.add_command(label="Show Pictures", command=self.show_pictures, compound='right')

    def create_pic(self):
        win = AddPic(self._win, self._client, self)

    def show_tags(self):
        win = TagsPicturesUser(self._win, self._client)

    def show_pictures(self):
        win = PicturesShowMode(self._pictures_data)

    def create_video(self):
        win = CreateVideo(self._client, self)
