from tkinter import *
from tkinter import colorchooser
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from tkinter import messagebox


class SendEmail(object):
    def __init__(self, parent):
        self._parent = parent
        self._sender_email_label = Label(self._parent, text="Your gmail: ")
        self._sender_email_label.place(x=20, y=20)
        self._sender_email_entry = Entry(self._parent, width=30)
        self._sender_email_entry.place(x=100, y=20)

        self._sender_password_label = Label(self._parent, text="Your gmail password: ")
        self._sender_password_label.place(x=20, y=50)
        self._sender_password_entry = Entry(self._parent, width=30, show='*')
        self._sender_password_entry.place(x=140, y=50)

        self._change_font_color = Button(self._parent, width=20,
                                         text="Change font color", command=self.change_color)
        self._change_font_color.place(x=500, y=20)

        self._change_font_color = Button(self._parent, width=20,
                                         text="Send->", command=self.send_mail)
        self._change_font_color.place(x=500, y=80)

        self._text = Text(self._parent,
            bg="light yellow",
            font=("Ink Free", 15),
            width=20,
            height=8,
            padx=20,
            pady=20)
        self._text.place(x=20, y=80)

    def change_color(self):
        color = colorchooser.askcolor()
        print(color)
        hex_color = color[1]
        self._text.config(fg=hex_color)

    def send_mail(self):
        dest_email = "mosheb2000@gmail.com"
        send_email = self._sender_email_entry.get()
        password = self._sender_password_entry.get()
        mail_content = self._text.get("1.0", END)

        if send_email == "":
            messagebox.showerror('ERROR', 'You must fill the email field')
            return
        elif password == "":
            messagebox.showerror('ERROR', 'You must fill the password field')
            return

        try:
            message = MIMEMultipart()
            message['From'] = send_email
            message['To'] = dest_email
            message['Subject'] = 'Reflection about the gallery app'   #The subject line
            #The body and the attachments for the mail
            message.attach(MIMEText(mail_content, 'plain'))
            #Create SMTP session for sending the mail
            session = smtplib.SMTP('smtp.gmail.com', 587) #use gmail with port
            session.starttls() #enable security
            session.login(send_email, password) #login with mail_id and password
            text = message.as_string()
            session.sendmail(send_email, dest_email, text)
            session.quit()
            print('Mail Sent')
            messagebox.showinfo('Message', 'Message has be sent')
        except Exception as e:
            messagebox.showinfo('Pay attention', 'Check if your email is exist\n'
                                                 'or check the security licence in\n'
                                                 'your google account')
