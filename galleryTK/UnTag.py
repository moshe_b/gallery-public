from tkinter import *
from PIL import ImageTk, Image
from tkinter import messagebox


class UnTagUser(object):
    def __init__(self, parent, client, pic_name):
        self._win = Toplevel()
        self._pic_name = pic_name
        self._parent_win = parent
        self._sock = client
        self._win.title('Un Tag User')
        self._win.geometry("250x200")
        self._win.iconphoto(False, PhotoImage(file='Assets/gallery_logo.png'))
        self._user_icon = ImageTk.PhotoImage(Image.open('Assets/user_icon.png').resize((50, 50)))
        self._user_id_label = Label(self._win, text='User Id: ')
        self._user_id_label.grid(row=0, column=0)
        self._user_id_entry = Entry(self._win, width=25)
        self._user_id_entry.grid(row=0, column=1)
        self._button_create = Button(self._win, text='Un Tag-', width=20, command=self.un_tag_user).grid(row=1, column=0, columnspan=2, padx=10, pady=10)
        self._label_user = Label(self._win)
        self._label_user.image = self._user_icon
        self._label_user.config(image=self._user_icon)
        self._label_user.grid(row=2, column=0)
        self._win.mainloop()

    def un_tag_user(self):
        user_id = str(self._user_id_entry.get())
        if not user_id.isnumeric():
            messagebox.showerror('ERROR', 'User id must be integer')
            return
        data_to_server = [self._pic_name, user_id]
        code, data_from_server = self._sock.recv_and_send(12, data_to_server)
        if data_from_server[0] == "ERROR":
            messagebox.showerror('ERROR', data_from_server[1])
        else:
            messagebox.showinfo('Tagged', f'The user[{user_id}] un tagged successfully')


