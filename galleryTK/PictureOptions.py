from tkinter import *
from TagUser import TagUser
from ShowPictureWin import ShowPicture
from UnTag import UnTagUser
from listTags import *
from ChangePermissions import  ChangePermissions

class PictureOptions(object):
    def __init__(self, pic_data, client):
        self._win = Toplevel()
        self._pic_name = pic_data[1]
        self._pic_data = pic_data
        self._client = client
        self._win.title(f'Picture {pic_data[1]} Options')
        self._win.geometry("350x350")
        self._var = IntVar()
        self._message = Label(self._win, text='Choose you option down bellow')
        self._message.place(x=70, y=10)
        self._tag_user = Radiobutton(self._win, text='Tag User', variable=self._var, value=1)
        self._tag_user.place(x=5, y=50)
        self._untag_user = Radiobutton(self._win, text='UnTag User', variable=self._var, value=2)
        self._untag_user.place(x=5, y=90)
        self._untag_user = Radiobutton(self._win, text='List Tags', variable=self._var, value=4)
        self._untag_user.place(x=5, y=130)
        self._edit_picture = Radiobutton(self._win, text='Edit Picture', variable=self._var, value=3)
        self._edit_picture.place(x=5, y=170)
        self._edit_picture = Radiobutton(self._win, text='Change permissions', variable=self._var, value=5)
        self._edit_picture.place(x=5, y=210)
        self._choice = Button(self._win, text='Go->', command=self.go)
        self._choice.place(x=10, y=250)
        self._win.mainloop()

    def go(self):
        choice = self._var.get()
        print(choice)
        if choice == 1:
            win = TagUser(self._win, self._client, self._pic_name)
        elif choice == 2:
            win = UnTagUser(self._win, self._client, self._pic_name)
        elif choice == 3:
            print('3 options')
            data = 'Name: ' + self._pic_data[1] + "\n" + "Location: " + self._pic_data[2] + "\n" + "Creation Date: " + self._pic_data[3]
            message = messagebox.showinfo(f'Picture {data[1]}', data)
            if message == "ok":
                self._win.withdraw()
                new_win = ShowPicture(self._client, self._pic_data)
        elif choice == 4:
            win = ListTags(self._client, self._win, self._pic_name)
        elif choice == 5:
            win = ChangePermissions(self._pic_data, self._client)
